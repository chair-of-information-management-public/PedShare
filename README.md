## PedShare ##

PedShare ist die Sharing-Plattform kleiner bis mittelgroßer Unternehmen, die ihren Mitarbeitenden die Möglichkeit bieten, ihre Dienst- und Privatfahrten umweltschonend zu tätigen. So können alle durch ein elektrounterstütztes Fahrrad nicht nur nachhaltiger, aber auch gesünder von A nach B kommen.

Dieses Repository enthält die Ergebnisse des PedShare Projektes:
* Client Applikation: Smartphone App auf Ionic Basis zur Buchung und Nutzung von E-Bikes
* Server Applikation: Google Firebase Functions zur Bereitstellung der Plattformfunktionalität
* Projektdokumentation: Zusammenfassung der Projekterkenntnisse zum Aufbau eines Pedelec-Sharing-Systems in kleinen und mittleren Unternehmen

Das PedShare Projekt wurde vom 01.01.2019-31.05.2022 von der Smart Mobility Research Group (Lehrstuhl für Informationsmangement, Georg-August-Universität Göttingen) zusammen mit den kooperierenden Unternehmen HNA, Eckold GmbH, Hoff GmbH und Werk-statt-Schule e.V. durchgeführt und erhielt eine Förderung vom Bundesministerium für Umwelt, Naturschutz und nukleare Sicherheit. 

Weitere Informationen unter https://pedshare.uni-goettingen.de

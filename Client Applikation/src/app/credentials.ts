/* Firebase Config der PedSare App */
export const firebaseConfig = {
    apiKey: '% insert here %',
    authDomain: '% insert here %',
    databaseURL: '% insert here %',
    projectId: '% insert here %',
    storageBucket: '% insert here %',
    messagingSenderId: '% insert here %',
    appId: '% insert here %'
  };

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastService } from './toast.service';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Platform } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(public fireStore: AngularFirestore, private router: Router, private toastService: ToastService, private platform: Platform, private angularFireAuthModul: AngularFireAuth) { }

  registerUser(email: string, password: string, companyId: string, accountEnabled: boolean, name: string) {
    this.angularFireAuthModul.auth.createUserWithEmailAndPassword(email, password).then(userCreated => {
      
      this.fireStore.collection<AngularFirestoreDocument>('users').doc(userCreated.user.uid).set({
        uid: userCreated.user.uid,
        email: userCreated.user.email,
        enableRanking: false,
        accountEnabled: accountEnabled,
        companyId: companyId,
        tutorialCompleted:  false,
        name: name //todo: Check if already exists
      }).then(() => {
        this.logoutUser(); // Logout da Nutzer nach Registration automatisch eingeloggt ist
        console.log("Object written");
        userCreated.user.sendEmailVerification();
        this.router.navigate(['/login']);   
        if(accountEnabled)
          this.toastService.presentClosableToast('Es wurde eine E-Mail zur Verifizierung an Deine E-Mail Adresse geschickt.', 'success');
        else
          this.toastService.presentClosableToast('Es wurde eine E-Mail zur Verifizierung an Deine E-Mail Adresse geschickt. Bevor Du PedShare nutzen kannst, muss Dein Unternehmen Deinen Account freigeben. Hierüber wirst Du per E-Mail informiert.', 'success', 10000);
        }).catch(err => {this.toastService.presentClosableToast("Beim Anlegen des Accounts ist ein Fehler aufgetreten.", 'danger')});
    }).catch(err => {
      switch (err.code) {
        case 'auth/email-already-in-use': {
          this.toastService.presentClosableToast('Es existiert bereits ein Account mit dieser E-Mail Adresse.', 'danger');
          break;
        }
        default: {
          this.toastService.presentClosableToast('Es ist ein unerwarteter Fehler aufgetreten.', 'danger');
          break;
        }
      }
    });
  }
  loginUser(email: string, password: string) {
   // firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then(result =>{
    this.angularFireAuthModul.auth.signInWithEmailAndPassword(email, password).then(res => {
      if (res.user.emailVerified) { // Email verifiziert
        return this.fireStore.collection<AngularFirestoreDocument>('users').doc(res.user.uid).ref.get().then(user => {
            
          if(!user.data().accountEnabled)
          {
            console.log(user.data);
            this.logoutUser();
            this.toastService.presentClosableToast('Da Du eine private E-Mail Adresse angegeben hast, muss Dein Account erst von Deinem Unternehmen freigegeben werden. Hierüber wirst Du per E-Mail informiert.', 'danger', 10000);
          }
          
          if(user.data().accountEnabled && user.data().tutorialCompleted)
            {
                  this.router.navigate(['/home']);
           } 
           if(user.data().accountEnabled && !user.data().tutorialCompleted) {
                  this.router.navigate(['/tutorial']);        
            }
          })
      } else { // Email nicht verifiziert
        this.logoutUser(); // Logout da Email nicht verifiziert
        this.toastService.presentClosableToast('Du musst Deine E-Mail Adresse bestätigen, bevor Du Dich einloggen kannst. Prüfe Deinen Spam-Ordner, falls du noch keine Nachricht bekommen hast.', 'danger', 10000);
      }
    })
      .catch(err => {
        switch (err.code) {
          case 'auth/user-not-found': {
            this.toastService.presentClosableToast('Es existiert kein Account mit dieser E-Mail Adresse.', 'danger');
            break;
          }
          case 'auth/wrong-password': {
            this.toastService.presentClosableToast('Falsches Passwort.', 'danger');
            break;
          }
          default: {
            this.toastService.presentClosableToast(err.message, 'danger');
            break;
          }
        }
      });
  //  });
  }
  resetPassword(email: string) {
    this.angularFireAuthModul.auth.sendPasswordResetEmail(email).then(res => {
      this.toastService.presentClosableToast('Eine E-Mail mit einem neuen Passwort wurde verschickt.', 'success');
    })
      .catch(err => {
        switch (err.code) {
          case 'auth/user-not-found': {
            this.toastService.presentClosableToast('Es existiert kein Account mit dieser E-Mail Adresse.', 'danger');
          }
        }
      });
  }
  changePassword(newPassword: string) {
    this.angularFireAuthModul.auth.currentUser.updatePassword(newPassword).then(res => {
      this.toastService.presentClosableToast('Dein Passwort wurde erfolgreich geändert.', 'success');
    })
      .catch(err => {
        this.toastService.presentClosableToast('Es ist ein unbekannter Fehler aufgetreten.', 'danger');
      });
  }
  deleteUser() {
    this.fireStore.collection('users').doc(this.getCurrentUserId()).delete().then(res => {
      this.angularFireAuthModul.auth.currentUser.delete().then(result => {
        this.router.navigate(['/login']);
        this.toastService.presentClosableToast('Dein Account wurde erfolgreich gelöscht.', 'success');
      })
        .catch(err => {
          this.toastService.presentClosableToast(err.code, 'danger');
        });
    })
    .catch(err => {
      this.toastService.presentClosableToast(err.code, 'danger');
    });
  }
  logoutUser() {
    return this.angularFireAuthModul.auth.signOut();
  }
  getCurrentUserId() {
    return this.angularFireAuthModul.auth.currentUser.uid;
  }
  getCurrentUser() {
    return this.angularFireAuthModul.auth.currentUser;
  }

}

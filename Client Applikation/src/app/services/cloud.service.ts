import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { environment} from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import * as moment from 'moment';
@Injectable({
  providedIn: 'root'
})
export class CloudService {

  constructor(private authService: AuthService, private http: HttpClient,private fireStore: AngularFirestore) {  moment.locale('de'); }


    /**
   * Returns all available bicycles for the given params
   * @param params startstationId, endstationId, userId, companyId, startdate, enddate. Dateformat: DD.MM.YYYY. hh:mm
   */
  getAvailableBicycles(startstationId, endstationId, startdate:moment.Moment, enddate:moment.Moment, companyId ) {
    let params = new HttpParams().set('startstationId', startstationId).set
    ('endstationId', endstationId).set('startdate', startdate.format('DD.MM.YYYY HH:mm'))
    .set('enddate', enddate.format('DD.MM.YYYY HH:mm')).set('companyId', companyId);
    return this.http.get(environment.serverAdress + 'getAvailableBicyles', {params}).toPromise();
  }

  /**
   * Pauses a booking
   * @param bookingId unique ID of a booking
   * @param enddate 
   */
  
   //TODO: Prüfen ob Datenbankaufruf im Backend stattfinden kann
  pauseBooking(bookingId: string, enddate: string) {
    return Promise.all([
      this.http.post(environment.serverAdress + 'attachTripToBooking', {bookingId, enddate}).toPromise(),  
      this.fireStore.collection('bookings').doc(bookingId).update({status: 'paused'})
    ])
  }

  /**
   * Calls the cloud function makeBooking to store booking in firestore (if possible)
   * @param startdate start of the booking, format "DD.MM.YYYY hh:mm"
   * @param enddate end of the booking, format "DD.MM.YYYY hh:mm"
   * @param startStation Id of the startStation
   * ToDo: Make complete
   */
  makeBooking(StartDate:moment.Moment, EndDate:moment.Moment, startstationId: string, endstationId: string, bicycleId: string) {
    const uid = this.authService.getCurrentUserId();
    const startdate =  StartDate.format('DD.MM.YYYY HH:mm');
    const enddate = EndDate.format('DD.MM.YYYY HH:mm');
    return this.http.post(environment.serverAdress + 'makeBooking', {startdate, enddate, uid, startstationId, endstationId, bicycleId}).toPromise();
  }


  /**
   * Deletes a booking based on a bookingId
   * @param bookingId unique ID of a booking
   */
  deleteBooking(bookingId: string) {
    return this.http.post(environment.serverAdress + 'deleteBooking', {bookingId});
  }
  /**
   * Edits a booking
   * @param bookingId unique ID of a booking
   */
  editBooking(bookingId: string) {
    return this.http.post(environment.serverAdress + 'editBooking', {bookingId});
  }

  isUpdatePossible(startstation: string, endstation: string, start: string, end: string, bicycleId: string, bookingId: string){
    let params = new HttpParams().set("startstation", startstation).set("endstation", endstation)
    .set("start", start).set("end", end)
    .set("bicycleId", bicycleId).set("bookingId", bookingId);
    return this.http.get(environment.serverAdress + 'isBikeAvailableTest', {params}).toPromise();
  }

  updateBooking(startstation: string, endstation: string, start: string, end: string, bicycleId: string, bookingId: string, endstationName: string){
    const uid = this.authService.getCurrentUserId();
    return this.http.post(environment.serverAdress + 'updateBooking', {startstation, endstation, start, end, bicycleId, bookingId, endstationName, uid}).toPromise();
  }


  /**
   * Ends an active booking
   * @param bookingId unique ID of a booking
   * @param enddate 
   */
  endBooking(bookingId: string, enddate: string)
  {
      return this.http.post(environment.serverAdress + 'endBooking', {bookingId, enddate}).toPromise(); 
  }

  /**
   * Ends an active booking
   * @param bookingId unique ID of a booking
   * @param enddate 
   */
  endBookingV2(bookingId: string, enddate: string,  end_lat: string, end_lng: string)
     {
         return this.http.post(environment.serverAdress + 'endBooking', {bookingId, enddate, end_lat, end_lng}).toPromise(); 
     }

  // Ends an active booking via persistance -> can be used, in case client is offline
  endBookingPersistence(bookingId: string, enddate: moment.Moment){
    return this.fireStore.collection('bookings').doc(bookingId).update({status: 'finished', enddate: enddate.toDate(), reSynchronize: true});
  }

  // Ends an active booking via persistance -> can be used, in case client is offline
   endBookingPersistenceV2(bookingId: string, enddate: moment.Moment, end_lat: string, end_lng: string, ){
    return this.fireStore.collection('bookings').doc(bookingId).update({status: 'finished', enddate: enddate.toDate(), reSynchronize: true, end_lat, end_lng});
  }

  /**
   * Extends an active booking
   * @param bookingId unique ID of a booking
   * @param newEnddate 
   */
  extendBooking(bookingId: string, newEnddate: string) {
    return this.http.post(environment.serverAdress + 'extendBooking', {bookingId, newEnddate}).toPromise();
  }
}

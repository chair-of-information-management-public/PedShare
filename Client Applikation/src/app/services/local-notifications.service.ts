import { Injectable } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Injectable({
  providedIn: 'root'
})
export class LocalNotificationsService {

  constructor(private localNotification: LocalNotifications) { }

  scheduleNotification(title: string, text:string, triggerDate:Date){
    console.log("Scheduled Notification for " + triggerDate.toString());
    this.localNotification.schedule(
      {
      title: title,
      text: text,
      trigger: { at: triggerDate},
      vibrate: true
    });
  }
}

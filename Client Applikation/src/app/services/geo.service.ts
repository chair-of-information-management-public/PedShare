import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

@Injectable({
  providedIn: 'root'
})
export class GeoService {
  constructor(private diagnostic: Diagnostic, private geolocation: Geolocation) { }

  getLocation(): Promise<any> {
    return this.geolocation.getCurrentPosition({timeout:5000})
      .then(location => {return {success: true, lng: String(location.coords.longitude), lat: String(location.coords.latitude)}})
      .catch(err => {return {success: false, msg: err}})
  };
}

import { Injectable, ɵConsole } from '@angular/core';
import { BLE } from '@ionic-native/ble/ngx';
import * as aesjs from 'aes-js';
import { checkAvailability } from '@ionic-native/core';
import { DatabaseService } from 'src/app/services/database.service';
import { Platform } from '@ionic/angular';
import { CloudService } from 'src/app/services/cloud.service';
import * as sha1 from 'js-sha1';

@Injectable({
  providedIn: 'root'
})
export class ILockItService {

  constructor(private ble: BLE, private cloudService: CloudService, private database: DatabaseService, private platform: Platform) { }

  connected: boolean = false;
  lockState: number = 0;
  dummylock: boolean = false;

  connectionTimeoutMs: number = 10000;
  pollLimitReached: boolean = false;
  name: string;
  connectId: string;

  userKey: Uint8Array;
  seed: Uint8Array;
  activateLockCounter = 0;


  lockControlServiceUUID: string = "% insert here %";
  authenticationCharacteristicUUID: string = "% insert here %";
  statusCharacteristicUUID: string = "% insert here %";
  activeLockCharacteriticUUID: string = "% insert here %";

  statusMap = {
    0x00: "Lock is open",
    0x01: "Lock is closed",
    0x03: "Lock could not close because of movement",
    0x04: "Lock could not open because locking bolt was blocked",
    0x05: "Lock could not close because locking bolt was blocked"
  }

  bikeTokenMap = {
    "firebase key": "token",
    // % insert here %
  }

  bikeLockMap = {
    "firebase key": "BLE Name",
    // % insert here %
  }

  timer(ms) {
    return new Promise(res => setTimeout(res, ms));
  }

  //needs to be called everytime the lock is used
  async _ensureConnection(): Promise<boolean> {
    return this.ble.isEnabled().then(() => {
      return this.ble.isConnected(this.connectId).then(() => {
        //device already connected
        this.connected = true;
        return true;
      }, async () => {
        //device was not connected -> try to establish connection
        this.connected = false;
        this._establishConnection();
        this.pollLimitReached = false;
        setTimeout(() => { this.pollLimitReached = true }, this.connectionTimeoutMs);

        while (!this.connected && !this.pollLimitReached) {
          await this.timer(500);
        };
        return this.connected;
      });
    }).catch(() => { return false });//Bluetooth is not enabled
  };

  _bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
  }


  //Established authenticated connection to lock
  _establishConnection(): void {
    this.ble.startScan([]).subscribe(device => {
      if (device.name == this.name) {
        console.log("Found bluetooth lock " + device.name);
        this.connectId = device.id;
        this.ble.stopScan();
        this.ble.connect(this.connectId).subscribe(
          //Connection was established
          peripheralData => {
            this.ble.write(device.id, this.lockControlServiceUUID, this.authenticationCharacteristicUUID, this.seed.buffer).then(response => {
              console.log("Wrote Seed " + this.seed);
              this.ble.read(device.id, this.lockControlServiceUUID, this.authenticationCharacteristicUUID).then(response => {
                const encryptedChallenge = new Uint8Array(response);
                console.log("Received ecrypted Challenge " + encryptedChallenge);
                var aesEcb = new aesjs.ModeOfOperation.ecb(this.userKey);
                var challenge = aesEcb.decrypt(encryptedChallenge);
                console.log("Challenge is: " + challenge);
                challenge[15] += 1 //Todo Check what happens with Carry :-D
                console.log("Modified Challenge: " + challenge);
                var encryptedModified = aesEcb.encrypt(challenge);
                console.log("Crypted Challenge: " + encryptedModified);
                this.ble.write(device.id, this.lockControlServiceUUID, this.authenticationCharacteristicUUID, encryptedModified.buffer).then(response => {
                  this.connected = true;
                  this.activateLockCounter = 0;
                  this.updateLockStatus(); //to make sure that initial state is stored
                  this.ble.startNotification(this.connectId, this.lockControlServiceUUID, this.statusCharacteristicUUID).subscribe(response => {
                    this.lockState = new Uint8Array(response[0])[0];
                    console.log("Lock status update received: " + this.statusMap[this.lockState]);
                  });
                }).catch(err => { console.log("Bluetooth Write Error :" + err) });
              }).catch(err => { console.log("Bluetooth Read Error :" + err) });
            }).catch(err => { console.log("Bluetooth Error: " + err); });
          },
          //Disconnected
          peripheralData => {
            this.connected = false;
            console.log('Device disconnected');
          });
      }
    });

    setTimeout(() => {
      this.ble.stopScan();
    }, this.connectionTimeoutMs);
  }


  //public functions begin here///////////////////////////////////////////////////////////////////////////////////////

  //Has to be called whenever a new lock should be used with the service
  initService(bikeId: string) {
    if (!(bikeId in this.bikeTokenMap)) {
      this.dummylock = true;
    }
    else {
      this.dummylock = false;
      this.name = this.bikeLockMap[bikeId];
      let internKey = new Uint8Array(16);
      this.seed = new Uint8Array(16);
      for (let i = 0; i < 16; i++) {
        var hexString = "0x" + this.bikeTokenMap[bikeId].substring(i * 2, i * 2 + 2);
        internKey[i] = parseInt(hexString);
        this.seed[i] = Math.ceil(Math.random() * 255)
      }
      var tempKey = new Uint8Array(32);
      tempKey.set(this.seed)
      tempKey.set(internKey, this.seed.length)

      var adminKey = sha1.array(tempKey);
      adminKey = adminKey.concat([0, 0, 0, 0])

      var userKey = sha1.array(adminKey)
      this.userKey = userKey.concat([0, 0, 0, 0])
    }
  }


  disconnect() {
    if (this.connected) {
      console.log("Disconnected programmaticaly")
      this.ble.disconnect(this.connectId);
      this.connected = false;
    }
  }

  updateLockStatus() {
    this._ensureConnection().then(ensured => {
      if (ensured) {
        this.ble.read(this.connectId, this.lockControlServiceUUID, this.statusCharacteristicUUID).then(response => {
          this.lockState = new Uint8Array(response[0])[0];
        });
      }
    });
  }

  async unlockBike(): Promise<any> {
    if (this.dummylock) {
      await this.timer(500);
      return Promise.resolve({ success: true, lockState: 0x00 })
    }
    else {
      return this._ensureConnection().then(ensured => {
        if (ensured) {
          this.activateLockCounter += 1;
          let msg = new Uint8Array(16);

          const mask = 0x000000FF;
          msg[0] = this.activateLockCounter & mask;
          msg[1] = this.activateLockCounter >> 8;
          msg[2] = 0x01;

          var aesEcb = new aesjs.ModeOfOperation.ecb(this.userKey);
          const encryptedMsg = aesEcb.encrypt(msg);

          return this.ble.write(this.connectId, this.lockControlServiceUUID, this.activeLockCharacteriticUUID, encryptedMsg.buffer).then(async response => {
            //start polling of state (max 15s); return state
            var pollCounter = 0;
            var pollLimit = 30; //30*500 = 15s
            //As long as lock is not open
            while ((this.lockState != 0x00) && (pollCounter < pollLimit)) {
              pollCounter += 1;
              await this.timer(500);
            };
            this.disconnect();
            return { success: true, lockState: this.lockState };
          })
        }
        else {
          return { success: false }
        }
      });

    }

  }

  async lockBike(): Promise<any> {
    if (this.dummylock) {
      await this.timer(500);
      return Promise.resolve({ success: true, lockState: 0x01 })
    }
    else {
      return this._ensureConnection().then(ensured => {
        if (ensured) {
          this.activateLockCounter += 1;
          let msg = new Uint8Array(16);
          msg[0] = this.activateLockCounter;
          msg[2] = 0x02;

          var aesEcb = new aesjs.ModeOfOperation.ecb(this.userKey);
          const encryptedMsg = aesEcb.encrypt(msg);

          return this.ble.write(this.connectId, this.lockControlServiceUUID, this.activeLockCharacteriticUUID, encryptedMsg.buffer).then(async response => {
            //start polling of state (max 15s); return state
            var pollCounter = 0;
            var pollLimit = 30; //30*500 = 15s
            //As long as lock is not open
            while ((this.lockState != 0x01) && (pollCounter < pollLimit)) {
              pollCounter += 1;
              await this.timer(500);
            };
            this.disconnect();
            return { success: true, lockState: this.lockState };
          });
        }
        else {
          return { success: false }
        }
      });
    }
  }

  stopAlarm(): Promise<any> {
    return this._ensureConnection().then(ensured => {
      if (ensured) {
        return this.ble.read(this.connectId, this.lockControlServiceUUID, this.statusCharacteristicUUID).then(response => {
          console.log(response);
          const statuscode = new Uint32Array(response)[0];
          return { success: true, statuscode: statuscode, meaning: this.statusMap[statuscode] };
        })
      }
      else {
        return { success: false }
      }
    });
  }
}
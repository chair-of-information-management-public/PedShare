import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { BehaviorSubject, Observable } from 'rxjs';
import { Platform } from '@ionic/angular';
import { ToastService } from './toast.service';

export enum ConnectionStatus {
  Online,
  Offline
}

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  private status: BehaviorSubject<ConnectionStatus> = new BehaviorSubject(ConnectionStatus.Offline);

  constructor(private network: Network, private toastService: ToastService, private plt: Platform) {
    this.plt.ready().then(() => {
      this.initializeNetworkEvents();
      const status =  this.network.type !== 'none' ? ConnectionStatus.Online : ConnectionStatus.Offline;
      this.status.next(status);
    });
  }

  public initializeNetworkEvents() {
    this.network.onDisconnect().subscribe(() => {
      if (this.status.getValue() === ConnectionStatus.Online) {
        this.updateNetworkStatus(ConnectionStatus.Offline);
      }
    });
    this.network.onConnect().subscribe(() => {
      if (this.status.getValue() === ConnectionStatus.Offline) {
        this.updateNetworkStatus(ConnectionStatus.Online);
      }
    });
  }
  /**
   * updates the Network Status on change
   * @param status Connection Status
   */
  private async updateNetworkStatus(status: ConnectionStatus) {
    this.status.next(status);
    const connection = status === ConnectionStatus.Offline ? 'Offline' : 'Online';
    if (connection === 'Offline') {
      this.toastService.presentClosableToast( `Es wurde keine Internetverbindung erkannt! Du bist ${connection}.`, 'warning');
    } else {
      this.toastService.presentClosableToast(`Es wurde eine Internetverbindung erkannt. Du bist wieder ${connection}.`, 'success');
    }
  }
  public onNetworkChange(): Observable<ConnectionStatus> {
    return this.status.asObservable();
  }
/**
 * Returns the current Network Status
 */
  public getCurrentNetworkStatus(): ConnectionStatus {
    return this.status.getValue();
  }
}

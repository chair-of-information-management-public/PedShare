import { TestBed } from '@angular/core/testing';

import { ILockItService } from './ilockit.service';

describe('IlockitService', () => {
  let service: ILockItService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ILockItService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

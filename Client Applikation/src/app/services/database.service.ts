import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection, QueryDocumentSnapshot } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';

import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import * as firebase from 'firebase/app';
import * as moment from 'moment';
import 'moment/locale/de';

moment.locale('de');


@Injectable({
  providedIn: 'root'
})

/**
 * Class that handles all direct database requests
 */
export class DatabaseService {

  constructor(private fireStore: AngularFirestore, private authService: AuthService, private http: HttpClient, private localNotifications: LocalNotifications,
    )  { }

/**----------------------------
 * USERS
 */

/**
 * Adds a user entry in the users Table
 * @param user
 */
createUser(user: any) {
  return this.fireStore.collection('users').doc(user.uid).set({
    activated: false,
    uid: user.uid,
    email: user.email,
    weekview: false, 
    tutorialCompleted: false
  });
}




/**
 * Returns the currently logged in user of the users Collection
 */
getUser() {
  return this.getUserById(this.authService.getCurrentUserId());
}

getUserById(uid) {
  return this.fireStore.collection<AngularFirestoreDocument>('users').doc(uid).ref.get().then(user => {
    let currentUser = user.data();
    currentUser.id = user.id;
    currentUser.ref = user.ref;
    currentUser['distanceInKm'] = currentUser.totalDistanceInM/1000.0;
    if (currentUser.achievements){
      currentUser.achievements.totalDistanceInKm = currentUser.achievements.totalDistanceInM / 1000.0
      currentUser.achievements.monthlyDistanceInKm = currentUser.achievements.monthlyDistanceInM / 1000.0
    }
     
    return currentUser;
  });
}

/**----------------------------
 * BICYCLES AND BICYCLESTATIONS
 */

/**
 * Returns all bicycle stations that belong to one companyID
 */
  getBicycleStations(companyId: string) {
    return this.fireStore.collection<AngularFirestoreCollection>('bicycleStations').ref.where('companyId', '==', companyId).get().then(stationSnapshots => {
      var stations = []
      stationSnapshots.docs.forEach(stationSnapshot => {
        stations.push(this.unpackBicycleStation(stationSnapshot));
        })
      return stations;
      });
  }

  unpackBicycleStation(bicycleStationSnapshot: QueryDocumentSnapshot<any>){
    let bicycleStation = bicycleStationSnapshot.data();
    bicycleStation.id = bicycleStationSnapshot.id;
    return bicycleStation;
  }

/**
 * Returns all bicycles that belong to one companyID
 */
  getBicycles(companyId: string){
    return this.fireStore.collection<AngularFirestoreCollection>('bicycles').ref.where('companyId', '==', companyId).get().then(bikesSnapshot => {
      var bikes = []
      bikesSnapshot.docs.forEach(bikeSnapshot => {
        bikes.push(this.unpackBicycle(bikeSnapshot));
      })
      return bikes;
    });
  }

  /**
   * Returns a single bicycle based on a provided bicycleId
   * @param id 
   */
  getBicycle(id: string){
    return this.fireStore.collection<AngularFirestoreCollection>('bicycles').doc(id).get().toPromise().then(bikeSnapshot => {
      return this.unpackBicycle(bikeSnapshot);
    });
  }
  /**
   * Unpacks a snapshot of a bicycle and returns the object
   * @param bicycleSnapshot Snapshot of a bicycle that is unpacked
   */
  unpackBicycle(bicycleSnpashot: QueryDocumentSnapshot<any>){
    let bicycle = bicycleSnpashot.data();
    bicycle.id = bicycleSnpashot.id;
    bicycle.ref = bicycleSnpashot.ref;
    return bicycle;
  }

  /**----------------------------
   * BOOKINGS
   */

  /**
   * Returns all bookings for a user as an array
   */
  getBookings() {
    return this.fireStore.collection('bookings').ref.where('uid', '==', this.authService.getCurrentUserId()).get().then(bookingsSnapshot => {
       let bookings = [];
       bookingsSnapshot.forEach(bookingSnapshot => {        
         bookings.push(this.unpackBooking(bookingSnapshot));
       });
       return bookings;
   });
   }

   getBooking(bookingId: string) {
    return this.fireStore.collection('bookings').ref.doc(bookingId).get().then(bookingSnapshot => {
       return this.unpackBooking(bookingSnapshot);
    });
  }

  /**
   * Returns last finished and open bookings to display for mobility manager 
  */
  async getBookingsByBike(bikeId: string){
    let BookingSnaphots = await this.fireStore.collection('bookings').ref.where('bicycleId', '==', bikeId).orderBy("enddate", "desc").limit(10).get();
    let bookings = [];
    BookingSnaphots.forEach(bookingSnapshot => {        
      bookings.push(this.unpackBooking(bookingSnapshot));
    });
    return bookings;
  }
 
   /**
    * Unpacks a snapshot ofa booking and returns it as an object
    * @param bookingSnapshot Snapshot of a booking returned by FireStore
    */
  unpackBooking(bookingSnapshot: QueryDocumentSnapshot<any>) {
    let booking = bookingSnapshot.data();
    booking.id =  bookingSnapshot.id;
    booking.ref = bookingSnapshot.ref;
    booking.startdate = moment(booking.startdate.toDate());
    booking.enddate = moment(booking.enddate.toDate());
    booking.totalDistanceInKM = booking.totalDistanceInM/1000;
    booking.totalDurationHHMM = this.getTimeFromMinutes(booking.totalDurationInM);
    return booking;
   }

  /**
   * Returns all future bookings for  a user in a list, ordered by enddate.
   */
  getFutureBookings() {
    return this.fireStore.collection('bookings').ref.where('uid', '==', this.authService.getCurrentUserId())
    .where('status', '==', 'planned').where('enddate', '>',  moment().toDate()).orderBy("enddate").get().then(bookings => { //order by enddate is required by firebase, altough startdate would be more correct
      let bookingList = [];
      bookings.forEach(bookingSnapshot => {
        bookingList.push(this.unpackBooking(bookingSnapshot));       
      });
      return bookingList;
    });
  }

  /**
   * Returns all current and active bookings of a user
   */
  getActiveBooking() {
    return this.fireStore.collection('bookings').ref.where('uid', '==', this.authService.getCurrentUserId())
      .where('status', 'in', ['active', 'paused']).get().then(bookings =>
        {
          if (bookings.size!=0)
            return this.unpackBooking(bookings.docs[0]);
          else 
            return null
        })
  }

  /**
   * Resumes a booking
   * @param bookingId 
   */
  resumeBooking(bookingId: string) {
    return Promise.all([
      this.fireStore.collection('bookings').doc(bookingId).update({status: 'active'}),
      this.fireStore.collection('bookings').doc(bookingId).collection("trips").add({start: moment().toDate()})
    ])
  }

  /**
   * Starts a booking
   * @param bookingId 
   * @param pickupDate 
   */
  startBooking(bookingId: string, pickupDate) {
    return Promise.all([
      this.fireStore.collection('bookings').doc(bookingId).update({status: 'active', startdate: pickupDate}),
      this.fireStore.collection('bookings').doc(bookingId).collection("trips").add({start: pickupDate})]
      );
  }

   /**
   * Starts a booking
   * @param bookingId 
   * @param pickupDate 
   */
    startBookingV2(bookingId: string, pickupDate, start_lat: string, start_lng: string) {
      return Promise.all([
        this.fireStore.collection('bookings').doc(bookingId).update({status: 'active', startdate: pickupDate, start_lat, start_lng}),
        this.fireStore.collection('bookings').doc(bookingId).collection("trips").add({start: pickupDate})]
        );
    }


  /**
   * Calculates the time from minutes
   * @param minutes 
   */
  getTimeFromMinutes(minutes){
    if (minutes < 0)
      return "0:00 h"
    
    let minutesDiff = minutes % 60;
    const hoursDiff = Math.floor(minutes / 60);
    if (minutesDiff < 10)
      return hoursDiff + ":0" + minutesDiff + " h";
    else
      return hoursDiff + ":" + minutesDiff + " h";
  }

  /**----------------------------
   * COMPANIES
   */

   /**
    * Returns a list of all companies
    */
  getCompanies() {
    return this.fireStore.collection('companies').ref.get().then(companiesSnapshot => {
      let companies = [];
      companiesSnapshot.forEach(companySnapshot => {
        companies.push(this.unpackCompany(companySnapshot));
      })
      return companies;
    })
  }

  /**
   * Returns a single company based on a provided companyId
   * @param companyId 
   */
  getCompany(companyId:string) {
    return this.fireStore.collection('companies').ref.doc(companyId).get().then(companySnapshot => {
      return this.unpackCompany(companySnapshot);
    })
  }


   /**
    * Unpacks a company snapshot and returns it as an object
    * @param companySnapshot 
    */
  unpackCompany(companySnapshot: QueryDocumentSnapshot<any>) {
    let company = companySnapshot.data();
    company.ref = companySnapshot.ref; 
    company.id = companySnapshot.id;
    return company;
   }

   /**
    * Returns a ranking list for a given user / company
    * @param companyId 
    */
  getOverallRanking(companyId) {
      return this.fireStore.collection<AngularFirestoreCollection>('users').ref.where('companyId', '==', companyId).where('enableRanking', '==', true)
      .orderBy('totalco2SavingsInKg', 'desc').limit(10).get().then(rankingSnapshot => {
        let ranking = []
        rankingSnapshot.forEach(rankSnapshot => {
          let userData = rankSnapshot.data();
          userData.distanceInKm = userData.totalDistanceInM/1000.0;
          if(userData.distanceInKm > 0)
            ranking.push(userData);
        })
        return ranking;
      })
  }

  getMonthlyRanking(companyId) {
    return this.fireStore.collection<AngularFirestoreCollection>('users').ref.where('companyId', '==', companyId).where('enableRanking', '==', true)
    .orderBy('monthlyco2SavingsInKg', 'desc').limit(10).get().then(rankingSnapshot => {
      let ranking = []
      rankingSnapshot.forEach(rankSnapshot => {
        let userData = rankSnapshot.data();
        userData.distanceInKm = userData.monthlyDistanceInM/1000.0;
        if(userData.distanceInKm > 0)
          ranking.push(userData);
      })
      return ranking;
    })
}

  /**
   * Returns all surveys in the collection
   */
  getSurveys(){
    return this.fireStore.collection('surveys').ref.where('online', '==', true).get().then(surveysSnapshot => {
      let surveys = [];
      surveysSnapshot.forEach(surveySnapshot => {
        surveys.push(this.unpackSurvey(surveySnapshot));
      })
      return surveys;
    })
  }
 /**
   * unpacks a survey snapshot
   */
  unpackSurvey(surveySnapshot: QueryDocumentSnapshot<any>) {
    let survey = surveySnapshot.data();
    survey.ref = surveySnapshot.ref; 
    survey.id = surveySnapshot.id;
    survey.questions.forEach((question, i) => {
      if(question.type=="selectQuestion"){
        if (!question.hasOwnProperty("answers"))
          survey.questions[i].answers = ["Stimme nicht zu", "Stimme eher nicht zu", "Weder noch", "Stimme eher zu", "Stimme zu"]
      }
    })
    return survey;
   }

      /**
   * update a bookings time frame
   * @param bookingId 
   * @param startDate
   * @param endDate
   */
  updateBooking(bookingId: string, startDate, endDate) {
    return Promise.all([
      this.fireStore.collection('bookings').doc(bookingId).update({ startdate: startDate, enddate:endDate})]
      );
  }

  
    /**
   * Sends a users survey submission to the database
   */

   //Todo in Promise.all
   sendSubmission(submission: any) {

    return this.fireStore.collection('surveySubmissions').add({
      surveyId: submission.surveyId,
      answers: submission.answers,
      timestamp: firebase.firestore.FieldValue.serverTimestamp()

    }).then(succes =>  {
      let fetchedUser;
      this.getUser().then(user => {
        fetchedUser = user;
        if (fetchedUser.completedSurveys) {
          const doc = this.fireStore.collection("users").doc(this.authService.getCurrentUserId());
          doc.update({
            completedSurveys: firebase.firestore.FieldValue.arrayUnion(submission.surveyId)
          });
        }
        else {
          const doc = this.fireStore.collection("users").doc(this.authService.getCurrentUserId());
          doc.update({
            completedSurveys: firebase.firestore.FieldValue.arrayUnion(submission.surveyId)
          });
        }
      });
    });
  }


  /**
   * PREFERENCES/SETTINGS
   */

  /**
   * Sets the tutorial to complete and updates the user profile
   */
  completeTutorial(){
    const completed = true;
    return this.fireStore.collection('users').doc(this.authService.getCurrentUserId()).update({tutorialCompleted:completed});
  }

  /**
   * Sets the weekview for a user and updates the user profile
   * @param weekviewValue boolean
   */
  setWeekView(weekviewValue: boolean) {
    return this.fireStore.collection('users').doc(this.authService.getCurrentUserId()).update({weekview: weekviewValue});
  }
  
  /**
   * Allows push notifications and updates the user profile
   * @param allowPushNotifications boolean
   */
  setAllowPushNotifications(allowPushNotifications: boolean) {
    if (allowPushNotifications === false) {
      this.localNotifications.clearAll(); // Cancel all scheduled Push Notifications
    }
    return this.fireStore.collection('users').doc(this.authService.getCurrentUserId()).update({allowPushNotifications});
  }

  /**
   * Changes the ranking setting and updates the user profile
   * @param enableRanking boolean
   */
  changeRankingSetting(enableRanking: boolean) {
    return this.fireStore.collection('users').doc(this.authService.getCurrentUserId()).update({enableRanking});
  }

  /**
   * Changes the username of the user and updates the user profile
   * @param name string
   */
  changeUsername(name: string){
    return this.fireStore.collection('users').doc(this.authService.getCurrentUserId()).update({name});
  }
}


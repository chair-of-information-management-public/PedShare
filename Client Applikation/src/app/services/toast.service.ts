import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class ToastService {
  toast: any = null;

  constructor(private toastController: ToastController, private loadingController: LoadingController,
              private firestore: AngularFirestore, private platform: Platform) { }
// Toasts ---------------------------------------------------------------------------------------------------------------
  /**
   * Creates a Toast
   * @param text : text of the Toast
   * @param style : style of the toast
   */
  presentToast(text: string, style: string, duration: number = 5000, position: string = 'top') {
    const toastData = {
      color: style,
      message: text,
      duration: duration,
      position: position
    };
    this.showToast(toastData);
  }
  /**
   * Creates a cloasable toast
   * @param text text of the toast
   * @param style style of the toast
   */
  presentClosableToast(text: string, style: string, duration: number = 5000, position: string = 'top' ) {
    const toastData = {
      color: style,
      message: text,
      showCloseButton: true,
      closeButtonText: 'X',
      position: position,
      duration: duration
    };
    this.showToast(toastData);
  }
  /**
   * Shows the created Toast on the screen
   * @param data Created Toast
   */
  async showToast(data: any) {
    if (this.toast) {
      this.toast.dismiss(); // Entfernt bereits vorhandenen toast
    }
    this.toast = await this.toastController.create(data);
    return await this.toast.present();
  }
// Loading Circle -----------------------------------------------------------------------------------------------
  async presentLoading(message: string) {
    const loading = await this.loadingController.create({
      message,
    });
    await loading.present();
  }
  async dismissLoader(){
    this.loadingController.dismiss();
  }
  // Push Notifications------------------------------------------------------------------------------------------
  async getToken(userId) {
    // let token;

    // if (this.platform.is('android')) {
    //   token = await this.firebase.getToken();
    // }

    // if (this.platform.is('ios')) {
    //   token = await this.firebase.getToken();
    //   await this.firebase.grantPermission();
    // }
    // this.saveToken(token, userId);
  }

  private saveToken(token, userId) {
    if (!token) { return; }

    const devicesRef = this.firestore.collection('devices');
    const data = {
      token,
      userId
    };

    return devicesRef.doc(token).set(data);
  }

  onNotifications() {
    //return this.firebase.onNotificationOpen();
  }
}

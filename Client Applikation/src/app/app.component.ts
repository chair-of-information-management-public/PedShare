import { Component, OnInit } from '@angular/core';

import { NavController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import * as firebase from 'firebase/app';
import { firebaseConfig } from './credentials';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { ToastService } from './services/toast.service';
import { DatabaseService } from './services/database.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  sideMenuContent: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private authService: AuthService,
    private toastService: ToastService,
    private databaseService: DatabaseService,
    private fireStore: AngularFirestore,
    private navCtrl: NavController
  ) {
    this.initializeApp();
    if (!firebase.apps.length) { // To avoid calling instantitation more than once
      firebase.initializeApp(firebaseConfig);
    }
    this.sideMenu();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // this.statusBar.overlaysWebView(true);
      // this.statusBar.backgroundColorByHexString('#1f2933');
      //this.statusBar.styleBlackOpaque();
      this.splashScreen.hide();
    });
    firebase.firestore().enablePersistence()  // Enable Offline Mode
    .catch(err => {
      if (err.code === 'failed-precondition') {
          // Multiple tabs open, persistence can only be enabled
          // in one tab at a a time.
          // ..."
        this.toastService.presentClosableToast('Offline Version nicht verfügbar. Eventuell sind mehrere Tabs offen.', 'warning');
        } else if (err.code === 'unimplemented') {
         this.toastService.presentClosableToast('Alte Browserversion! Es werden nicht alle Funktionalitäten unterstützt.', 'warning');      }
});
  }

  sideMenu() {
    this.sideMenuContent =
      [
        {
          title: 'Home',
          url: '/home',
          icon: 'home'
        },
        {
          title: 'Profil',
          url: '/profile',
          icon: 'person'
        },
        {
          title: 'Buchungen',
          url: '/bookings',
          icon: 'calendar'
        },
        {
          title: 'Erfolge',
          url: '/ranking',
          icon: 'trophy'
        },
        {
          title: 'Forschung',
          url: '/survey',
          icon: 'chatbubbles'
        },
        {
          title: 'Mobility Manager',
          url: '/admin',
          icon: 'hammer'
        },
        {
          title: 'Info',
          url: '/info',
          icon: 'help-circle'
        }
      ];
  }
 async logoutUser() {
    this.authService.logoutUser().then(async res => {
      this.navCtrl.navigateRoot('/login');
      this.toastService.presentClosableToast('Logout erfolgreich', 'success');
     // if (this.platform.is('cordova')) {
     //   await this.fcm.getToken().then(async token => {
     //   this.fireStore.collection('devices').doc(String(token)).delete();
     // }
     //);
   // }
    }).catch(err => {
      this.toastService.presentClosableToast(err.message, 'danger');
    });
  }
}

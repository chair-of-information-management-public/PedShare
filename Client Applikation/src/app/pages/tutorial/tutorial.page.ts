import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { MenuController, IonSlides } from '@ionic/angular';


import { DatabaseService } from 'src/app/services/database.service';

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage implements OnInit{

  @ViewChild('slides', { static: true }) slides: IonSlides;

  constructor(
    public menu: MenuController,
    public router: Router,

    public databaseService: DatabaseService
  ) {  }

  ngOnInit(){

  }

  startApp() {
    this.router
      .navigateByUrl('/home', { replaceUrl: true })
      .then(() => this.databaseService.completeTutorial());
  }
  
}
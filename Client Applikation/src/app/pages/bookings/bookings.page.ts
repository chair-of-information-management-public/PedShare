import { Component, ElementRef, Renderer2, AfterViewChecked, OnInit, DoCheck, ChangeDetectorRef, ViewChild, HostListener } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';
import * as moment from 'moment';
import { AlertController, IonCard } from '@ionic/angular';
import { ModalController, Platform } from '@ionic/angular';
import { UpdateBookingPage } from 'src/app/components/update-booking/update-booking.page'
import { CalendarComponentOptions } from 'ion2-calendar';
import { ToastService } from 'src/app/services/toast.service';
import { CloudService } from 'src/app/services/cloud.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})

export class BookingsPage implements OnInit {
  // UI 
  bookingsToDisplay = [];
  selectedDate = moment().format('DD.MM.YYYY');

  calendarOptions: CalendarComponentOptions = {
    color: 'primary',
    from: new Date(1),
    monthPickerFormat: ["Jan", "Feb", "Mrz", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
    weekdays: ['S', 'M', 'D', 'M', 'D', 'F', 'S'],
    weekStart: 1
  }; 

  // DATA 
  user;
  bookings = [];

  @ViewChild("calendar", { read: ElementRef }) private calendar: ElementRef;
  private mutationObserver: MutationObserver;

  
  constructor(private database: DatabaseService, private router: Router, private modalController: ModalController,
    private cloudService: CloudService, private toastService: ToastService, private alertController: AlertController,
    private renderer: Renderer2, private eleRef: ElementRef, private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(){
    // Ion-Calender has not load new day elements, when month change gets triggered;
    // Observer is used to identify DOM-Changes as consequence of month-change
    // New Labels are loaded, as soon as DOM-Changes have been detected
    this.mutationObserver = new MutationObserver((mutations: MutationRecord[]): void => {
      let monthChangeDetected = false;
      mutations.forEach(mutation => {
        if(mutation.target["className"] == "days-box"){
          monthChangeDetected = true;
        }
      });
      if (monthChangeDetected){
        console.log("Month Change in Calendar detected!");
        this.loadBadges();
      }
    });
    this.mutationObserver.observe(this.calendar.nativeElement, {
      childList: true,
      subtree: true
    });

  }

  async ionViewWillEnter(){
    this.user = await this.database.getUser();
    this.bookings = await this.database.getBookings();
    this.daySelect();
    this.loadBadges();
  }

  /**
   * Adds Badges with the amound of bookings for every day in the calendar
   */
  loadBadges() {
    //1. Remove existing badges
    let badges = this.eleRef.nativeElement.querySelectorAll('ion-badge.cl');
    badges.forEach(badge => {
      badge.parentNode.removeChild(badge);
    });

    //2. Load Badges according to Bookings
    let dayButtons = this.eleRef.nativeElement.querySelectorAll('button.days-btn');
    dayButtons.forEach(dayButton => {
      const dayLabel = dayButton.attributes[3].nodeValue; // return something like 'May 27, 2021'
      const day = moment(dayLabel, "MMMM DD, YYYY", "en");
      // console.log(dayLabel);
      // console.log(day.month());
      const badge = this.renderer.createElement('ion-badge');
      this.renderer.addClass(badge, 'cl');
      let valueBadge = 0;
      this.bookings.forEach(booking => {    
        if (booking.startdate.isSame(day, 'day')) {
          valueBadge++;
        }
      });
      if (valueBadge > 0) {
        const badgeValue = this.renderer.createText(valueBadge.toString());
        this.renderer.appendChild(badge, badgeValue);
        this.renderer.appendChild(dayButton, badge);
      }
    });
  }

  async updateBooking(selectedBooking) {
    const modal = await this.modalController.create({
      component: UpdateBookingPage,
      componentProps: {
        booking: selectedBooking,
        user: this.user
      }
    });

    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.submitted == true) {
      let index = this.bookings.findIndex(b => b.id == data.booking.id);
      this.bookings[index] = data.booking;
      this.daySelect();
    }
  }

  /**
   * Function which is triggered on model change of the calendar. Updates the bookingsToday array.
   */
  daySelect() {
    this.bookingsToDisplay = [];
    const selectedMoment = moment(this.selectedDate, 'DD.MM.YYYY');
    this.bookings.forEach(booking => {
      if (selectedMoment.isSameOrAfter(booking.startdate, 'day') && selectedMoment.isSameOrBefore(booking.enddate, 'day')){
        if (!booking.startdate.isSame(booking.enddate, 'day')){
          booking.multipleDays = true;
        }
        this.bookingsToDisplay.push(booking);
      }      
    });
  }

  async deleteBooking(selectedBooking) {
    const alert = await this.alertController.create({
      header: 'Löschen',
      message: 'Möchtest Du die ausgewählte Buchung löschen?',
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Löschen',
          handler: (data: any) => {
            this.toastService.presentLoading('Das Löschen der Buchung erfolgt..');
            this.cloudService.deleteBooking(selectedBooking.id).toPromise().then(message => {
              if (message['status'] == 'success') {
                this.toastService.dismissLoader();
                this.toastService.presentToast('Die Buchung wurde gelöscht', 'success', 2000, 'bottom');
                this.bookings.splice(this.bookings.findIndex(b => b.id == selectedBooking.id), 1); // Removes Booking from loaded Bookings
                this.loadBadges();
              }
              this.daySelect();
              // Bei einem Fehler wird eine Fehlermeldung ausgegeben
              // tslint:disable-next-line: triple-equals
              if (message['status'] == 'error') {
                this.toastService.dismissLoader();
                this.toastService.presentToast('Es ist ein unerwarteter Fehler aufgetreten', 'warning', 2000, 'bottom');
              }
            });
          }
        }
      ]
    });

    await alert.present();
  }
}
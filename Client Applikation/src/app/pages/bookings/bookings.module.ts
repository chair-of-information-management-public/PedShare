import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { CalendarModule } from 'ion2-calendar';

import { BookingsPage } from './bookings.page';
import { DateFormatPipe } from 'src/app/pipes/date-format.pipe';
import { WeekviewComponent } from '../../components/weekview/weekview.component';
import { SharedModule } from '../../components/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: BookingsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalendarModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [BookingsPage, DateFormatPipe, WeekviewComponent],
  providers: [DateFormatPipe]
})
export class BookingsPageModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminPageRoutingModule } from './admin-routing.module';

import { DateFormatPipe } from 'src/app/pipes/date-format.pipe';

import { AdminPage } from './admin.page';
import { DisplayUserModalComponent } from '../../components/display-user-modal/display-user-modal.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminPageRoutingModule,
  ],
  declarations: [AdminPage, DateFormatPipe, DisplayUserModalComponent]
})
export class AdminPageModule {}

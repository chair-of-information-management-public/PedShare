import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../../services/database.service';
import { ToastService } from '../../services/toast.service';
import { ILockItService } from 'src/app/services/ilockit.service';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ModalController } from '@ionic/angular';
import { DisplayUserModalComponent } from '../../components/display-user-modal/display-user-modal.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {

  constructor(private databaseService: DatabaseService, private toastService: ToastService, 
    private iLockItService: ILockItService, private emailComposer: EmailComposer, private callNumber: CallNumber, 
    private modalController: ModalController) { }

  bikes = [];
  selectedBike;
  user;
  loading = true;
  company;
  loadBookings = false;
  bikeBookings = [];

  ngOnInit() {
    this.databaseService.getUser().then(async user => {
      this.user = user;
      if (this.user.isAdmin)
      {
        //ADMIN MODE -> Load Bikes & Bookings
        this.bikes = await this.databaseService.getBicycles(user.companyId);
        //this.selectedBike = this.bikes[0];
      }
      
      //NORMAL USER MODE -> LOAD COMPANY
      this.company = await this.databaseService.getCompany(user.companyId);
      this.loading = false;
    });
  }

  mailMobilityManager() {
        let email = {
          to: this.company.mobilityManagerMail,
          subject: 'Anfrage zur PedShare App',
          isHtml: true
        };
        this.emailComposer.open(email).catch(() => {
      this.toastService.presentClosableToast(`Beim Aufrufen der E-Mail App ist ein Problem aufgetreten. Du kannst manuell eine Nachricht an '${this.company.mobilityManagerMail}' senden.`, "danger");
    });
  }

  callMobilityManager() {
    this.callNumber.callNumber(this.company.mobilityManagerPhone, true)
      .then(res => console.log('Anruf erfolgreich', res))
      .catch(err => this.toastService.presentClosableToast(`Beim Aufrufen der Telefon App ist ein Problem aufgetreten. Du kannst manuell einen Anruf an '${this.company.mobilityManagerPhone}' tätigen.`, "danger"));
  }

  async initLockService() {
    this.loadBookings = true;
    if (this.selectedBike != undefined){
       this.iLockItService.initService(this.selectedBike);
    }
    
    this.bikeBookings = await this.databaseService.getBookingsByBike(this.selectedBike);
    this.loadBookings = false;
  }

  async lockBike() {
    if (this.selectedBike != undefined) {
      await this.toastService.presentLoading('Fahrrad wird verriegelt...');
      this.iLockItService.lockBike().then(status => {
        this.toastService.dismissLoader();
        if (status.success) {
          if (status.lockState != 0x01) {
            this.toastService.presentClosableToast("Das Schloss konnte leider nicht geschlossen werden. Möglicherweise ist es von einer Speiche blockiert.", "danger")
          }
        }
        else {
          this.toastService.presentClosableToast("Das Schloss konnte leider nicht geschlossen werden. Stelle sicher das Dein Bluetooth aktiviert ist und Du Dich in Reichweite des Fahrrades befindest.", "danger")
        }
      });
    }
    else {
      this.toastService.presentToast("Bitte wähle zunächst ein Fahrrad aus!", "danger");
    }
  }

  async unlockBike() {
    if (this.selectedBike != undefined) {
      await this.toastService.presentLoading('Fahrrad wird entriegelt...');
      this.iLockItService.unlockBike().then(status => {
        this.toastService.dismissLoader();
        if (status.success) {
          if (status.lockState != 0x00) {
            this.toastService.presentClosableToast("Das Schloss konnte leider nicht geöffnet werden. Möglicherweise muss der Akku von I-Lock-It extern geladen werden.", "danger")
          }
        }
        else {
          this.toastService.presentClosableToast("Das Schloss konnte leider nicht geöffnet werden. Stelle sicher das Dein Bluetooth aktiviert ist und Du Dich in Reichweite des Fahrrades befindest.", "danger")
        }
      });
    }
    else {
      this.toastService.presentToast("Bitte wähle zunächst ein Fahrrad aus!", "danger");
    }
  }

  async openUserModal(uid){
    console.log(uid);
    const modal = await this.modalController.create({
      component: DisplayUserModalComponent,
      componentProps: {
        'uid': uid,
        'company': this.company.name
      }
    });
    return await modal.present();
  }
}

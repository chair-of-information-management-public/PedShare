import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';
import { AlertController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { ModalController, Platform } from '@ionic/angular';
import { DatenschutzPage } from 'src/app/components/datenschutz/datenschutz.page';
import { TutorialPage } from 'src/app/components/tutorial/tutorial.page';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage {
  user: any;
  constructor(private authService: AuthService, private database: DatabaseService, private modalController: ModalController, private platform: Platform, private alertController: AlertController) { }

  ionViewWillEnter() {
    this.getUser();
  }
  getUser() {
     this.database.getUser().then(user => {
      this.user = user;
    });
  }
  changeUsername(name: string) {
    this.database.changeUsername(name);
  }
 
  changeRankingSetting(allow: boolean) {
    this.database.changeRankingSetting(allow);
  }
  /**
   * Reload side
   * @param event Event of content Refresher
   */
  doRefresh(event) {
    setTimeout(() => {
      this.getUser();
      event.target.complete();
    }, 500);
  }
  async presentDeleteAccountAlert() {
    const alert = await this.alertController.create({
      header: 'Account löschen',
      message: 'Bist Du sicher, dass Du Deinen Account löschen möchtest?',
      buttons: [
        {
          text: 'Bestätigen',
          cssClass: 'secondary',
          handler: () => {
            this.authService.deleteUser();
          }
        }, {
          text: 'Abbrechen',
          role: 'cancel',
        }
      ]
    });
    await alert.present();
  }
  async presentChangePasswordAlert() {
    const alert = await this.alertController.create({
      header: 'Passwort ändern',
      message: 'Um Dein Passwort zu ändern, gib Bitte Dein neues Passwort ein und bestätige es. ',
      inputs: [
        {
          name: 'newPassword',
          type: 'password',
          placeholder: 'Neues Passwort'
        },
        {
          name: 'confirmPassword',
          type: 'password',
          placeholder: 'Neues Passwort bestätigen'
        }
      ],
      buttons: [
        {
          text: 'Bestätigen',
          cssClass: 'secondary',
          handler: data => {
            if (data.newPassword.length < 6) {
              alert.message = 'Dein Passwort ist zu kurz.';
              return false;
            }
            if (data.newPassword !== data.confirmPassword) {
              alert.message = 'Die Passwörter stimmen nicht überein';
              return false;
            }
            this.authService.changePassword(data.newPassword);
          }
        }, {
          text: 'Abbrechen',
          role: 'cancel',
        }
      ]
    });
    await alert.present();
  }
  async openDataProtectionDeclaration() {
    const modal = await this.modalController.create({
      component: DatenschutzPage,
      componentProps: {
      }
    });
    modal.present();
    }


    async openTutorial() {
      const modal = await this.modalController.create({
      component: TutorialPage,
      componentProps: {
      }
    });
    modal.present();
    }
    
}

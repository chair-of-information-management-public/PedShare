import { Component, OnInit } from '@angular/core';
import { ViewWillEnter } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';
import { DatabaseService } from 'src/app/services/database.service';
import { AuthService } from '../../services/auth.service';
import { LoadingController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { DynSurveyModalPage } from 'src/app/components/dyn-survey-modal/dyn-survey-modal.page';


@Component({
  selector: 'app-survey',
  templateUrl: './survey.page.html',
  styleUrls: ['./survey.page.scss'],

})
export class SurveyPage implements OnInit{
  surveys = [];
  completedSurveys = [];
  offlineCompletedCount = 0;
  showSpinner : boolean;
  surveyStatus: String;
  user: any;

  private loading;

  constructor(private authService: AuthService, private router: Router,private loadingController: LoadingController, public databaseService: DatabaseService, public modalController: ModalController) {
    this.surveyStatus = "Lade Umfragen...";
    }

    ngOnInit() {
      this.showSpinner = true;
      
      this.databaseService.getUser().then(user =>{
        this.user = user;
        if(!user.hasOwnProperty("completedSurveys"))
          user.completedSurveys=[]
        this.databaseService.getSurveys().then(surveys => {
          surveys.forEach(survey => {
            if(user.completedSurveys.includes(survey.id))
              this.completedSurveys.push(survey);
            else 
              this.surveys.push(survey);
          });
          this.offlineCompletedCount = user.completedSurveys.length - this.completedSurveys.length;
          this.showSpinner = false;
        })
      })
    }

    async startSurvey(survey){
      delete survey.ref;//Workaround to fix Ionic History BUG, which occurs when ref is part of object
      const modal = await this.modalController.create({
        component: DynSurveyModalPage,
        componentProps: {
          survey: survey,
          user: this.user
        }
      });
      await modal.present();
      const { data } = await modal.onWillDismiss();
      if (data)
        if (data.submitted)
        {
          this.surveys = this.surveys.filter(s => (s.id != survey.id));
          this.completedSurveys.push(survey);
        }
    }
  }

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {
  public passwordResetForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private authService: AuthService) {
    this.passwordResetForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
    });
   }

  ngOnInit() {
  }
  resetPassword() {
    this.authService.resetPassword(this.passwordResetForm.value.email);
  }

}

import { Component, OnInit } from '@angular/core';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { DatabaseService } from '../../services/database.service';
import { ToastService } from '../../services/toast.service';
import { AuthService } from '../../services/auth.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  constructor(private emailComposer: EmailComposer, private toastService: ToastService, private databaseService: DatabaseService, private authService: AuthService, private iab:InAppBrowser) { }

  ngOnInit() {
  }

  openPedShareWebsite(){
    const browser = this.iab.create('https://pedshare.uni-goettingen.de/', '_system');
  }

  mailSupport(){
        let email = {
          to: "christoph.prinz@uni-goettingen.de",
          subject: 'Technische Anfrage zur PedShare App',
          body: `Sehr geehrtes PedShare-Team,<br/>bei der Nutzung der App ist mir folgendes aufgefallen: <br/>#Hier Nachricht eintippen#<br/><br/>Mit freundlichen Grüßen<br/><br/><hr>Informationen die zur Lösung des Problems beitragen können:<br/>Nutzer-Id: ${this.authService.getCurrentUserId()}<br/>`,
          isHtml: true
        };
        this.emailComposer.open(email).catch(() => {
          this.toastService.presentClosableToast(`Beim Aufrufen der E-Mail App ist ein Problem aufgetreten. Du kannst manuell eine Nachricht an 'christoph.prinz@uni-goettingen.de' senden.`, "danger");
    });
  }
}

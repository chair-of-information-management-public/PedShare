import { Component, OnInit, ViewChild } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';
import { IonInfiniteScroll } from '@ionic/angular';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';



@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.page.html',
  styleUrls: ['./ranking.page.scss'],
})
export class RankingPage implements OnInit {
  @ViewChild(IonInfiniteScroll, { static: true }) infiniteScroll: IonInfiniteScroll;
  user;
  monthly_ranking;
  total_ranking;
  loading: boolean;
  segment: string; // "monthly" or "total"

  constructor(private database: DatabaseService) { }

  async ngOnInit() {
    this.loading = true;
    await this.loadData();
    this.loading = false;
    this.segment="monthly";
  }

  async refreshData(event){
    await this.loadData();
    event.target.complete()
  }

  async loadData() {
    this.user = await this.database.getUser();
    this.total_ranking = await this.database.getOverallRanking(this.user.companyId);
    this.monthly_ranking = await this.database.getMonthlyRanking(this.user.companyId);
  }

  segmentChanged(value){
    console.log(value);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';

import { TripNotificationComponent } from 'src/app/components/trip-notification/trip-notification.component';
import { SharedModule } from '../../components/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes), SharedModule
  ], providers: [TripNotificationComponent],
  declarations: [HomePage, TripNotificationComponent ]
})
export class HomePageModule {}

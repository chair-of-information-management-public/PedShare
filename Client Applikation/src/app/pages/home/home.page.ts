import {Component, ElementRef, ViewChild, ChangeDetectorRef, OnInit} from '@angular/core';
import { GeoService } from 'src/app/services/geo.service';
import { Marker, GoogleMapsEvent, LatLng, GoogleMaps, GoogleMapOptions, Environment, MarkerOptions} from '@ionic-native/google-maps';
import { DatabaseService } from 'src/app/services/database.service';
import { ModalController, Platform } from '@ionic/angular';
import { MakeBookingModalPage } from 'src/app/components/make-booking-modal/make-booking-modal.page';
import { MenuController, NavController } from '@ionic/angular';
import { ILockItService } from 'src/app/services/ilockit.service';

import { CalendarComponentOptions } from 'ion2-calendar';
import * as moment from 'moment';
import 'moment/locale/de';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { ToastService } from 'src/app/services/toast.service';
import { CloudService } from 'src/app/services/cloud.service';
import { HttpParams } from '@angular/common/http';


export interface BicycleStation {
  position: {lat, lng};
  title: string;
  id: string;
}
export interface Bicycle {
  name: string;
  bicycleId: string;
  currentStation: string;
}
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  constructor(
    private toastService: ToastService,
    private cloudService: CloudService,
    private modalController: ModalController, private platform: Platform,
    private diagnostic: Diagnostic, private geoService: GeoService, private databaseService: DatabaseService,
    private changeDetectorRef: ChangeDetectorRef,
    private navCtrl: NavController, private menuCtrl: MenuController,
    private iLockItService: ILockItService) {}

  marker;
  locationEnabled : boolean;
  bookings = [];
  activeBooking;
  static title;
  map: any;
  longitude: any;
  latitude: any;
  location: LatLng;
  user: any;
  activeBicycleStation;
  googleMarkerStations  = [];
  allBicycleStations: Array<any>;
  isSelected: boolean;
  hideTripNotification: boolean = true;
  company;

   @ViewChild('selectStation', {static: false}) btn: ElementRef;

  calendarOptions: CalendarComponentOptions = {
    from: new Date(1)
  };

  @ViewChild('mapElement', {static: false}) mapElement;

  ngOnInit(): void {
    this.menuCtrl.enable(true);
    this.databaseService.getUser().then(user => {
      this.user = user;
      this.checkActiveBooking();
      this.databaseService.getCompany(user.companyId).then(company => {
        this.company = company;
        this.initMap(company);
      })
    });
  }
  checkActiveBooking()
  {
    Promise.all([
      this.databaseService.getActiveBooking().then(activeBooking => {   
        this.activeBooking = activeBooking;
        return true;
      }),
      this.databaseService.getFutureBookings().then(futureBookings => {
        futureBookings.forEach(booking => {
          if(booking.startdate < moment()){
            this.activeBooking = booking;
          }
        });
        this.bookings = futureBookings;
        return true;
      })
    ]).then(() => {
    if(this.activeBooking == null)
        this.hideTripNotification = true;
    else
    {
      this.hideTripNotification = false;
      this.iLockItService.initService(this.activeBooking.bicycleId);    
      //set timeout, which calls this again after booking ended -> to make sure that notification closes
      const now = moment();
      setTimeout(this.checkActiveBooking, this.activeBooking.enddate.diff(now, 'miliseconds') + 30000);
    }
      this.changeDetectorRef.detectChanges();
    })
  }

  // MAP FUNCTIONS
  // Initializing the map and setting a default location. Center could be replaced by method.
  initMap(company) {
    Environment.setEnv({
      API_KEY_FOR_BROWSER_RELEASE: '% insert here %',
      API_KEY_FOR_BROWSER_DEBUG:   '% insert here %'
    });
    const mapOptions: GoogleMapOptions = {
      controls: {
        compass: true,
        myLocationButton: true,
        myLocation: true,   // (blue dot)
        zoom: true          // android only
      },
      gestures: {
        scroll: true,
        tilt: true,
        zoom: true,
        rotate: true
      },
      camera: {
        target: {lat: Number.parseFloat(company.latitude), lng: Number.parseFloat(company.longitude)},
        zoom: 15
      },
    };
    this.map = GoogleMaps.create('mapElement', mapOptions);

    // Wait until map is ready (including that users accepted location permission)
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      this.geoService.getLocation().then(location => {
        if(location.success)
          this.map.setCameraTarget(location);
        else
          console.log("Location Error");
      })
  
      this.loadBicycleStations(company.id);
    })
  }

 loadBicycleStations(companyId) { 
  const iconBicycleGreen = {
    url: './assets/icon/stationGreen.png', // image url
    size: {
      width: 48,
      height: 72
    }
  };
  const iconBicycleRed = {
    url: './assets/icon/stationRed.png', // image url
    size: {
      width: 48,
      height: 72
    }
  };
  const iconBicycle = {
    url: './assets/icon/station.png', // image url
    size: {
      width: 48,
      height: 72
    }
  };
  
  this.allBicycleStations = []; //set global because it is required by booking-modal
  this.databaseService.getBicycleStations(companyId).then(bicycleStations => {
    this.allBicycleStations =  bicycleStations; 
    bicycleStations.forEach(bicycleStation => { 
        // Create Google Markers
        const lat = parseFloat(bicycleStation.latitude);
        const lng = parseFloat(bicycleStation.longitude);
        const position = {lat, lng};
        const optionMarker: MarkerOptions = {
          position,
          icon : iconBicycle,
        };
        const marker: Marker = this.map.addMarkerSync(optionMarker);

        // Loads availability label for station
          this.cloudService.getAvailableBicycles(
            bicycleStation.id, bicycleStation.id,  
            moment(), 
            moment().add('2', 'hour'),
            this.user.companyId
          ).then(response => {
            if (response['availableBicycles'].length === 0) {
              marker.setIcon(iconBicycleRed);
            } else
            {
              marker.setIcon(iconBicycleGreen); 
            }
          });     

        // Add Onclick Event for marker to open booking modal
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(event => {
            this.openModal(bicycleStation);
          });
        this.googleMarkerStations.push(marker);
      });
    });  
}

//Opens Booking Modal
async openModal(selectedStation) {
  const modal = await this.modalController.create({
    component: MakeBookingModalPage,
    componentProps: {
      allStations: this.allBicycleStations,
      pickUpStation: selectedStation,
      user: this.user
    }
  });
  modal.onDidDismiss().then(() => {
    this.checkActiveBooking();
  });
  return await modal.present();
  }  
}


import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { DatabaseService } from '../../services/database.service';
import { ToastService } from 'src/app/services/toast.service';
import { MenuController, NavController } from '@ionic/angular';
import { ModalController, Platform } from '@ionic/angular';
import { DatenschutzPage } from 'src/app/components/datenschutz/datenschutz.page';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public registerForm: FormGroup;
  private DSGVOCheck: boolean;
  constructor(private formBuilder: FormBuilder, private modalController: ModalController,
    private authService: AuthService, private databaseService: DatabaseService,
    private toastService: ToastService, public menuCtrl: MenuController, public navCtrl: NavController) { }

  companies;
  domainIdDict = {};
  domainCompanyNameDict = {};
  companyNameIdDict = {};


  ngOnInit() {
    this.DSGVOCheck = false;
    this.menuCtrl.enable(false);
    this.registerForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      name: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      passwordConfirm: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      dataPolicyConfirm: [false, Validators.pattern('true')],
      company: ['', Validators.required]
    }, {
      validator: this.checkMatchingPassword()
    });
    this.databaseService.getCompanies().then(companies => {
      this.companies = companies;
      companies.forEach(company => {
        this.companyNameIdDict[company.name] = company.id;
        company.domains.forEach(domain => {
            this.domainIdDict[domain] = company.id;
            this.domainCompanyNameDict[domain] = company.name; 
          })
      });
    })
  }
  checkMatchingPassword() {
    return (group: FormGroup) => {
      const passwordInput = group.controls.password,
        passwordConfirmationInput = group.controls.passwordConfirm;
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true });
      } else {
        return passwordConfirmationInput.setErrors(null);
      }
    };
  }

  preselectCompany(){
    const userDomain = this.registerForm.value.email.split('@')[1];
    if (Object.keys(this.domainCompanyNameDict).includes(userDomain)){
      this.registerForm.controls['company'].setValue(this.domainCompanyNameDict[userDomain]);
      this.registerForm.controls['company'].disable();
    }
    else{
      this.registerForm.controls['company'].enable();
    }
  }

  registerUser() {
    const userDomain = this.registerForm.value.email.split('@')[1];
    
    if (Object.keys(this.domainIdDict).includes(userDomain)){
      //domain => company.id
      this.authService.registerUser(this.registerForm.value.email, this.registerForm.value.password, this.domainIdDict[userDomain], true, this.registerForm.value.name);
    } else {
      //todo open modal with company //Start register
      this.authService.registerUser(this.registerForm.value.email, this.registerForm.value.password,  this.companyNameIdDict[this.registerForm.value.company], false, this.registerForm.value.name);
    }
  }

  async openModal() {
  
    const modal = await this.modalController.create({
      component: DatenschutzPage,
    });
    return await modal.present();
  }

}

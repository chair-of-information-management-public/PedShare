import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { MenuController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private authService: AuthService, public menuCtrl: MenuController, public navCtrl: NavController) {
    
   }

   ionViewWillEnter(){
      this.menuCtrl.enable(false);
   }
  
  ngOnInit() {
    this.menuCtrl.enable(false);

    this.loginForm = this.formBuilder.group({
      email: new FormControl('',[
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    });    
  }

  loginUser() {
    this.authService.loginUser(this.loginForm.value.email, this.loginForm.value.password);
  }

  get primEmail(){
    return this.loginForm.get('email');
    }
}

import { Component, OnInit, Input, AfterViewInit, AfterContentInit, AfterViewChecked, DoCheck } from '@angular/core';
import * as moment from 'moment';


@Component({
  selector: 'app-weekview',
  templateUrl: './weekview.component.html',
  styleUrls: ['./weekview.component.scss'],
})
export class WeekviewComponent implements OnInit, DoCheck {
  @Input() bookings;
  date: any;
  nextSevenDays = [];
  uhrzeitBeschriftung = ['7:00', '8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00' , '16:00' , '17:00', '18:00',
  '19:00'];
 bookingsInSelectedWeek = [[], [] , [] , [] , []];
 selectedBooking;
 initialCheckFinished = false;

  constructor() { }
  ngDoCheck() {
    this.checkForBookingsInSelectedWeek();
  }
  ngOnInit() {
    let currentDate = moment();

    this.date = currentDate.clone().startOf('isoWeek'); // Default Date is today
    console.log('ngOnInit: this date : ', this.date);
    this.getDaysOfWeek();
  }
  /**
   * Gets next 7 days of the currently selected date
   */
  getDaysOfWeek() {

    console.log('get days of week2:  ', this.date);
    this.nextSevenDays = [];
    this.nextSevenDays.push(moment(this.date));
    let dateTmp = this.date;
    for (let i = 0; i < 4; i++) {
      this.nextSevenDays.push(moment(dateTmp.add(1, 'days')));
    }
    var currentDate = moment(dateTmp);
    this.date = currentDate.clone().startOf('isoWeek');
    console.log("ende getdaysofweek:", this.date);
    this.checkForBookingsInSelectedWeek();
  }
  /**
   * Gets days from next week
   */
  getDaysNextWeek() {
    console.log(this.date);
    this.date = moment(this.date).add(7, 'days');
    console.log(this.date);
    this.getDaysOfWeek();
  }
/**
 * Gets days from previous week
 */
  getDaysPreviousWeek() {
    this.date.subtract(7, 'days');
    this.getDaysOfWeek();
  }
  showSelectedBooking(indexTag, indexBuchung) {
    this.selectedBooking = this.bookingsInSelectedWeek[indexTag][indexBuchung];
  }
  /**
   * Checks for Bookings of the selected days and adds them and leerbelegungen to the bookingsInSelectedWeek Array
   */
  checkForBookingsInSelectedWeek() {
    this.bookingsInSelectedWeek =  [[], [], [], [], []];
    this.bookings.forEach(booking => {
      if (booking.enddate.isBetween(this.nextSevenDays[0], this.nextSevenDays[4], 'day', '[]') ||  // Buchung im ausgewählten Zeitraum
          booking.startdate.isBetween(this.nextSevenDays[0], this.nextSevenDays[4], 'day', '[]')  ) {
          for (let i = 0; i < this.nextSevenDays.length; i++) {
            const day = this.nextSevenDays[i];
            const midnight = moment(booking.startdate.toDate()).clone().startOf('day');
            if (moment(booking.enddate.toDate()).day() === day.day() || moment(booking.startdate.toDate()).day() === day.day()) { // Buchung am entsprechenden Tag
              if (moment(booking.enddate.toDate()).day() === moment(booking.startdate.toDate()).day()) {   // Buchung nicht über Mitternacht
                this.bookingsInSelectedWeek[i].push({booking, start: moment(booking.startdate.toDate()).diff(midnight , 'minutes'),
                                                     duration: moment(booking.enddate.toDate()).diff(moment(booking.startdate.toDate()), 'minutes') ,
                                                     type: 'booking'});
              }
              if (moment(booking.startdate.toDate()).day() === day.day() && moment(booking.enddate.toDate()).day() !== day.day()) { // Nur Startdatum am Tag
                this.bookingsInSelectedWeek[i].push({booking, start: moment(booking.startdate.toDate()).diff(midnight, 'minutes'),
                                                    duration: midnight.diff(moment(booking.startdate.toDate()), 'minutes') ,
                                                    type: 'booking'});
              }
              if (moment.unix(booking.enddate.seconds).day() === day.day() && moment(booking.startdate.toDate()).day() !== day.day()) { // Nur Enddatum am Tag
                this.bookingsInSelectedWeek[i].push({booking, start: midnight.diff(moment(booking.enddate.toDate()).day(), 'minutes'),
                                                    duration: moment(booking.enddate.toDate()).diff(midnight, 'minutes') ,
                                                    type: 'booking'});
              }
            }
          }
      }
    });
    this.bookingsInSelectedWeek.forEach(day => { // Hinzufügen der Freiräume zwischen den Buchungen
      let added = 0;
      for (let i = 0; i < day.length; i++) {
        const buchung = day[i];
        if (i === 0) {  // Beginn Tag
          if (buchung.start !== 0) {
            day.unshift({start: 0, duration: buchung.start, type: 'empty'});
            added++;
          }
        }
        if (i === day.length - 1) { // Ende Tag
          if (buchung.start + buchung.duration < 1440) {
            day.push({start: buchung.start + buchung.duration, duration: 1440 - buchung.start + buchung.duration, type: 'empty'});
            added++;
          }
        }
        if (i < day.length - 1 && buchung.start + buchung.duration < day[i + 1].start) { // Alle anderen
          day.splice(i + 1 + added, 0, {start: buchung.start + buchung.duration, duration: day[i + 1].start - buchung.start + buchung.duration, type: 'empty'});
          added++;
        }
    }
      });
    }
}

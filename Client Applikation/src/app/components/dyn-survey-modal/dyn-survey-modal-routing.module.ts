import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DynSurveyModalPage } from './dyn-survey-modal.page';

const routes: Routes = [
  {
    path: '',
    component: DynSurveyModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DynSurveyModalPageRoutingModule {}

import { Component, OnInit, Input } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';
import { ToastService } from 'src/app/services/toast.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-dyn-survey-modal',
  templateUrl: './dyn-survey-modal.page.html',
  styleUrls: ['./dyn-survey-modal.page.scss'],
})
export class DynSurveyModalPage implements OnInit {
  @Input() survey: any;
  @Input() user: any;
  submissions = {};

  constructor( private toastService: ToastService, private databaseService: DatabaseService, public modalController: ModalController) { }

  ngOnInit() {
  }

  async sendSubmission(){
    await this.toastService.presentLoading('Die Umfrage wird versendet...');

    const finalSub: any = {
      surveyId: this.survey.id,
      answers: this.submissions,
    }
    if(Object.keys(this.submissions).length == this.survey.questions.length) {
      this.databaseService.sendSubmission(finalSub).then(success=>{
        this.toastService.dismissLoader();
        this.dismissModal(true);
      }),
      error=>{
        this.toastService.dismissLoader();
        this.toastService.presentToast('Die Umfrage konnte nicht abgeschickt werden, bitte versuche es erneut','danger')
      }
    }
    else{ //todo: only fields that are empty should have the "danger" property
      this.toastService.dismissLoader();
      this.survey.questions.forEach((question, index) => {
        if(!this.submissions[index]){
          var element = document.getElementById(index);
          element.classList.add("danger"); // dynamic class Name
          this.toastService.presentToast('Bitte beantworte alle Fragen','danger')
        }        
      });
    }
  }

  dismissModal(submitted=false) {
    this.modalController.dismiss({
      submitted: submitted
    });
  }

}

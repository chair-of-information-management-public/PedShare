import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DynSurveyModalPageRoutingModule } from './dyn-survey-modal-routing.module';

import { DynSurveyModalPage } from './dyn-survey-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DynSurveyModalPageRoutingModule
  ],
  declarations: [DynSurveyModalPage]
})
export class DynSurveyModalPageModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EndBookingModalPageRoutingModule } from './end-booking-modal-routing.module';

import { EndBookingModalPage } from './end-booking-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EndBookingModalPageRoutingModule
  ],
  declarations: [EndBookingModalPage]
})
export class EndBookingModalPageModule {}

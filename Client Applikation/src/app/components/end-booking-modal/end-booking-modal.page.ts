import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DatabaseService } from 'src/app/services/database.service';

@Component({
  selector: 'app-end-booking-modal',
  templateUrl: './end-booking-modal.page.html',
  styleUrls: ['./end-booking-modal.page.scss'],
})
export class EndBookingModalPage implements OnInit {

  @Input() booking: any;
  @Input() offline: boolean;

  // UI Controls
  loading:boolean = true;

  constructor(private modalController: ModalController, private dataService: DatabaseService) {

   }

  async ngOnInit() {
    if (!this.offline){
      this.booking = await this.dataService.getBooking(this.booking.id);
      if(!this.booking.hasOwnProperty("funfact"))
      {
        this.booking.funfact = "Es konnte leider kein Funfact geladen werden."
      }
    }
    this.loading = false;
  }

  closeFinishedBookingScreen() {
    this.modalController.dismiss({
      'dismissed': true,
    });
  }

}

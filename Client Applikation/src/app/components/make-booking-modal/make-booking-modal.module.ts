import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MakeBookingModalPage } from './make-booking-modal.page';

const routes: Routes = [
  {
    path: '',
    component: MakeBookingModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MakeBookingModalPage]
})
export class MakeBookingModalPageModule {}

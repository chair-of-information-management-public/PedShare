import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { NavParams, NavController,ModalController } from '@ionic/angular';
import { DatabaseService } from 'src/app/services/database.service';
import { ToastService } from 'src/app/services/toast.service';
import { AuthService } from 'src/app/services/auth.service';
import { HttpParams } from '@angular/common/http';
import { LocalNotificationsService } from 'src/app/services/local-notifications.service'
import { CloudService } from 'src/app/services/cloud.service';
import { ChangeDetectorRef } from '@angular/core';

import * as moment from 'moment';
import 'moment/locale/de';

moment.locale('de');


@Component({
  selector: 'app-modal',
  templateUrl: './make-booking-modal.page.html',
  styleUrls: ['./make-booking-modal.page.scss'],
})
export class MakeBookingModalPage implements OnInit {

  // Parameter übergeben von Homepage
  @Input() pickUpStation: any;
  @Input() allStations: any;
  @Input() user: any;
  
  // UserInputs
  pickupTime: string;
  dropOffTime: string;
  dropOffStation: any;
  selectedBicycle: any;

  //UI Controls
  now = moment().toISOString(true);
  inOneYear = moment().add(1, "year").toISOString(true);
  showSpinner: boolean = false;
  showConnectionError: boolean = false;
  availableBicycles: any = [];

  constructor(private navParams: NavParams, private authService: AuthService, private databaseService: DatabaseService,
              private toastService: ToastService, private cloudService: CloudService, private modalController: ModalController, 
              private localNotificationService: LocalNotificationsService, private changeRef: ChangeDetectorRef){
  }

  ngOnInit() {
    this.dropOffStation = this.pickUpStation;
    this.pickupTime = moment().toISOString(true);
    this.autoSetDropOffTime();
    this.getAvailableBikes();
  }

  autoSetDropOffTime(){
    this.dropOffTime = moment(this.pickupTime).add(2, 'hours').toISOString(true);
  }
 
  getAvailableBikes(){   
    this.showSpinner = true;
    this.availableBicycles = [];
    this.selectedBicycle = null;    
    this.cloudService.getAvailableBicycles(this.pickUpStation.id, this.dropOffStation.id, moment(this.pickupTime), moment(this.dropOffTime), this.user.companyId).then(response => {
      this.showConnectionError = true;
      if (response["status"]=="success")
      {
        this.showConnectionError = false;
        this.availableBicycles = response["availableBicycles"];
      }
      else{
        this.showConnectionError=true;
      }
      this.showSpinner = false;
  });
}

  // Schließt das Modal
  dismissModal() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  // Buchungsfunktion
  async makeBooking() {
    await this.toastService.presentLoading('Die Buchung erfolgt..');
    this.cloudService.makeBooking(moment(this.pickupTime), moment(this.dropOffTime), this.pickUpStation.id, this.dropOffStation.id, this.selectedBicycle).then(message => {
        this.toastService.dismissLoader();
        // Wenn die Buchung erfolgreich ist -> Success anzeigen
        if (message['status'] == 'success') {
          this.modalController.dismiss();
          this.localNotificationService.scheduleNotification("Deine PedShare Buchung beginnt...", "Dein E-Bike wartet an der Station '" + this.pickUpStation.name +  "' auf Dich. Viel Spaß!", moment(this.pickupTime).toDate());
        }
        // Bei einem Fehler wird eine Fehlermeldung ausgegeben
        if (message['status'] == 'error') {
          this.toastService.presentToast('Es ist ein Fehler aufgetreten, bitte versuche es erneut.', 'warning');
          this.modalController.dismiss();
        }
        }).catch(err => {
        this.toastService.dismissLoader();
        this.toastService.presentToast('Die Buchung ist fehlerhaft: ' + err, 'warning');
        });
    }
  }
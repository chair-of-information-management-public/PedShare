import { Component, OnInit, Input } from '@angular/core';
import { DatabaseService} from 'src/app/services/database.service';
import { ToastService } from '../../services/toast.service';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-display-user-modal',
  templateUrl: './display-user-modal.component.html',
  styleUrls: ['./display-user-modal.component.scss'],
})
export class DisplayUserModalComponent implements OnInit {  
  @Input() uid: string;
  @Input() company: string;
  user;

  constructor(private databaseService: DatabaseService, private emailComposer:EmailComposer, private toastService:ToastService, private modalController: ModalController) { }

  async ngOnInit() {
    this.user = await this.databaseService.getUserById(this.uid);
  }

  mailUser(mail){
    let email = {
          to: mail,
          subject: 'Anfrage zur Nutzung der PedShare App',
          isHtml: true
        };
        this.emailComposer.open(email).catch(() => {
      this.toastService.presentClosableToast(`Beim Aufrufen der E-Mail App ist ein Problem aufgetreten. Du kannst manuell eine Nachricht an '${this.user.email}' senden.`, "danger");
    });

  }

  dismissModal(){
    this.modalController.dismiss({});
  }
}

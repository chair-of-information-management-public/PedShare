import { Component, OnChanges, Input, ChangeDetectionStrategy, ViewChild, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';
import * as moment from 'moment';
import 'moment/locale/de';
import { ModalController } from '@ionic/angular';
import { ToastService } from 'src/app/services/toast.service';
import { GeoService } from 'src/app/services/geo.service';
import { ILockItService } from 'src/app/services/ilockit.service';
import { ChangeDetectorRef } from '@angular/core';
import { Platform, IonDatetime } from '@ionic/angular';
import { strict } from 'assert';
import { CloudService } from 'src/app/services/cloud.service';
import { EndBookingModalPage } from 'src/app/components/end-booking-modal/end-booking-modal.page'

@Component({
  selector: 'app-trip-notification',
  templateUrl: './trip-notification.component.html',
  styleUrls: ['./trip-notification.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class TripNotificationComponent implements OnChanges  {
  @Input('activeBooking') activeBooking;
  bookings = [];
  minutesTillNextBooking;
  timeLeft;
  newEnddate;
  
  hideActiveBookingScreen = true;
  hideUnlockScreen = true;  
  hideFinishedBookingScreen = true;
  loadFinishedBookingParameter = false;
  tripResultsAvailable = false;
  positionSubscription;
  trackedRoute = [];
  
  allowTracking = false;
  
  startedBookingAtTime;
  endedBookingAtTime;
  durationOfBooking;
 
  bikelocked = false;

  totalDistanceInKM = 0;
  totalco2Saved = 0;
  averageSpeed = 0;
  totalDurationHHMM = ""; //fromat is 0:00 h
  
  pickerMin = moment().toISOString(true);
  pickerMax = moment().add('1', 'year').toISOString(true);

  constructor(private database: DatabaseService, private modalController: ModalController, private toastService: ToastService, private cloudService: CloudService, private geoService: GeoService,private platform: Platform,private changeRef: ChangeDetectorRef, private iLockItService: ILockItService) {
   }

  ngOnChanges() {
    this.checkForShowTripComponent();
  }
  /**
   * Checks wheter the planned Booking or active Booking template should be displayed based on the status of the booking
   */
  checkForShowTripComponent() {
    if (this.activeBooking)
    {
      switch(this.activeBooking.status) {
        case "paused":
            this.bikelocked = true;  
            //includes case active  
        case "active":
          this.hideUnlockScreen = true;
          this.hideActiveBookingScreen = false;
          this.updateTimeLeft();
          this.pickerMin = this.activeBooking.enddate.toISOString(true);
          this.pickerMax = moment(this.activeBooking.enddate).add('1', 'year').toISOString(true);
          this.newEnddate = this.activeBooking.enddate.toISOString(true);
          console.log(this.pickerMin);
          console.log(this.newEnddate);
          break;
        case "planned":
          this.minutesTillNextBooking = this.activeBooking.startdate.diff(moment(), 'minutes');
          if (this.minutesTillNextBooking <= 5) { // Show Booking Template if next booking within 5 minutes
            this.hideActiveBookingScreen = true;
            this.hideUnlockScreen = false;
          }
          break;
      }
      this.changeRef.detectChanges();
    }
  }

  /**
   * Unlocks the bicycle
   */
  async startBooking() {   
    await this.toastService.presentLoading('Fahrrad wird entriegelt...');
    this.iLockItService.unlockBike().then(async status => {
      if(status.success){
        if(status.lockState == 0x00){
          //lock was opened successfull
          this.bikelocked = false;
          this.hideUnlockScreen = true;
          this.hideActiveBookingScreen = false;
          this.updateTimeLeft();
          this.startedBookingAtTime = moment().toDate();
          const pos = await this.geoService.getLocation();
          if (pos.success){
            await this.database.startBookingV2(this.activeBooking.id, moment().toDate(), pos.lat, pos.lng );
          }
          else{
            await this.database.startBooking(this.activeBooking.id, moment().toDate());
          }
          this.toastService.dismissLoader();
          this.changeRef.detectChanges();
        }
        else{
          this.toastService.dismissLoader();
          this.toastService.presentClosableToast("Das Schloss konnte leider nicht geöffnet werden. Möglicherweise muss der Akku von I-Lock-It extern geladen werden.", "danger")
        }
      }
      else{
        this.toastService.dismissLoader();
        this.toastService.presentClosableToast("Das Schloss konnte leider nicht geöffnet werden. Stellen Sie sicher das Bluetooth aktiviert ist und Sie sich in Reichweite des Fahrrades befinden.", "danger")
      }
    });
  }

  /**
   * Extends the currently active booking if possible
   */
   async extendBooking() {
    await this.toastService.presentLoading('Prüfe ob eine Verlängerung möglich ist...');
    this.cloudService.extendBooking(this.activeBooking.id, moment(this.newEnddate).format('DD.MM.YYYY HH:mm')).then(result => {
      if(result['status'] == 'success')
      {
          if(result['works'] == true)
          {
            this.pickerMin = moment(this.newEnddate).format("YYYY-MM-DDTHH:mm");    
            this.activeBooking.enddate = moment(this.newEnddate);
            let diff = this.activeBooking.enddate.diff(moment(), 'minutes');
            this.timeLeft = this.getTimeFromMinutes(diff);
            this.changeRef.detectChanges();
            this.toastService.presentClosableToast('Buchung erfolgreich verlängert!', 'success');
          }
          else
          {
             this.toastService.presentClosableToast(result['msg'], 'danger');
          }
      }
      else{
        this.toastService.presentClosableToast("Es ist ein technischer Fehler aufgetreten. Bitte später erneut versuchen.", 'danger');
      }
      this.toastService.dismissLoader();
    });
  }

  async pauseBooking(){
    await this.toastService.presentLoading('Fahrrad wird verriegelt...');    
    this.iLockItService.lockBike().then(async status => {
      if(status.success){
        if(status.lockState == 0x01){
          this.bikelocked = true;  
          this.toastService.dismissLoader();
          this.changeRef.detectChanges();
          await this.cloudService.pauseBooking(this.activeBooking.id, moment().format('DD.MM.YYYY HH:mm'));
        }
        else{
          this.toastService.dismissLoader();
          this.toastService.presentClosableToast("Das Schloss konnte leider nicht geschlossen werden. Möglicherweise ist es von einer Speiche blockiert.", "danger")
        }
      }
      else{
        this.toastService.dismissLoader();
        this.toastService.presentClosableToast("Das Schloss konnte leider nicht geschlossen werden. Stelle sicher dass Dein Bluetooth aktiviert ist und Du Dich in Reichweite des Fahrrades befindest.", "danger")
      }
    });
  }

  async resumeBooking(){
    await this.toastService.presentLoading('Fahrrad wird entriegelt...');
    this.iLockItService.unlockBike().then(async status => {
      if(status.success){
        if(status.lockState == 0x00){
            this.bikelocked = false;           
            this.toastService.dismissLoader();
            this.changeRef.detectChanges();
            await this.database.resumeBooking(this.activeBooking.id);
        }
        else{
          this.toastService.dismissLoader();
          this.toastService.presentClosableToast("Das Schloss konnte leider nicht geöffnet werden. Möglicherweise muss der Akku von I-Lock-It extern geladen werden.", "danger")
        }
      }
      else{
        this.toastService.dismissLoader();
        this.toastService.presentClosableToast("Das Schloss konnte leider nicht geöffnet werden. Stelle sicher dass Dein Bluetooth aktiviert ist und Du Dich in Reichweite des Fahrrades befindest.", "danger")
      }
    });
  }

  async endBooking() {
    await this.toastService.presentLoading('Fahrrad wird verriegelt...');    
 
    this.iLockItService.lockBike().then(async status => {
      if(status.success){
        if(status.lockState == 0x01){
          this.bikelocked = true; 
          let offline = false;
          const pos = await this.geoService.getLocation();
          try{
            if (pos.success){
              await this.cloudService.endBookingV2(this.activeBooking.id, moment().format('DD.MM.YYYY HH:mm:ss'), pos.lat, pos.lng);  
            }
            else{
              await this.cloudService.endBooking(this.activeBooking.id, moment().format('DD.MM.YYYY HH:mm:ss'));
            }
          }
          catch{
            console.log("Client offline");
            offline = true;
            if(pos.success){
              this.cloudService.endBookingPersistenceV2(this.activeBooking.id, moment(), pos.lat, pos.lng);
            }
            else{
              this.cloudService.endBookingPersistence(this.activeBooking.id, moment());
            }
           
          }
          finally{
            this.hideActiveBookingScreen = true;
            this.toastService.dismissLoader();
            this.changeRef.detectChanges();
            const modal = await this.modalController.create({
              component: EndBookingModalPage,
              componentProps: {
                booking: this.activeBooking,
                offline: offline
              }
            });
            await modal.present();
          }  
        }
        else{
          this.toastService.dismissLoader();
          this.toastService.presentClosableToast("Das Schloss konnte leider nicht geschlossen werden. Möglicherweise ist es von einer Speiche blockiert.", "danger")
        }
      }
      else{
        this.toastService.dismissLoader();
        this.toastService.presentClosableToast("Das Schloss konnte leider nicht geschlossen werden. Stelle sicher dass Dein Bluetooth aktiviert ist und Du Dich in Reichweite des Fahrrades befindest.", "danger")
      }
    });
  } 
    
  updateTimeLeft() {
    let diff = this.activeBooking.enddate.diff(moment(), 'minutes');
    this.timeLeft = this.getTimeFromMinutes(diff);
    this.changeRef.detectChanges();
    setTimeout( () => {
      this.updateTimeLeft();
    }, 60000);
  }

  getTimeFromMinutes(minutes){
    if (minutes < 0)
      return "0:00 h"
    
    let minutesDiff = minutes % 60;
    const hoursDiff = Math.floor(minutes / 60);
    if (minutesDiff < 10)
      return hoursDiff + ":0" + minutesDiff + " h";
    else
      return hoursDiff + ":" + minutesDiff + " h";
  }
}

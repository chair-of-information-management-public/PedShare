import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OfflineBannerComponent} from '../offline-banner/offline-banner.component';

/**
 * Adds the Components which are used on multiple pages to an Module
 */
@NgModule({
  declarations: [OfflineBannerComponent],
  imports: [
    CommonModule
  ],
  exports: [OfflineBannerComponent]
})
export class SharedModule { }

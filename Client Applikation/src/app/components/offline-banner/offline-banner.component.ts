import { Component, OnInit } from '@angular/core';
import { NetworkService, ConnectionStatus } from 'src/app/services/network.service';

@Component({
  selector: 'app-offline-banner',
  templateUrl: './offline-banner.component.html',
  styleUrls: ['./offline-banner.component.scss'],
})
export class OfflineBannerComponent implements OnInit {
  public offline: boolean ;
  constructor( private networkService: NetworkService) {}
  ngOnInit() {
    this.networkService.onNetworkChange().subscribe((status: ConnectionStatus) => { // Change of Networkstatus
      if (this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline) {
        this.offline = true;
      }
      if (this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online) {
        this.offline = false;
      }
    });
  }
}

import { Component, OnInit, Input } from '@angular/core';
import {  ModalController } from '@ionic/angular';
import { NavParams, NavController} from '@ionic/angular';

@Component({
  selector: 'app-datenschutz',
  templateUrl: './datenschutz.page.html',
  styleUrls: ['./datenschutz.page.scss'],
})
export class DatenschutzPage implements OnInit {

  @Input() DSGVOCheck;
  showCheck: boolean;

  constructor(private modalController: ModalController, private navParams: NavParams) { }

  ngOnInit() {
    this.showCheck = true;
    this.isDSGVOCheckRequested();
  }

 async closeModal() {
    await this.modalController.dismiss(this.DSGVOCheck);
  }

  isDSGVOCheckRequested(){
    if(this.DSGVOCheck === false){
      this.showCheck = false;
    }
  }
  agreeToDSGVO(){
   this.DSGVOCheck = true;
   this.closeModal();
  }
  declineDSGVO(){
    this.DSGVOCheck = false;
    this.closeModal();
  }

}

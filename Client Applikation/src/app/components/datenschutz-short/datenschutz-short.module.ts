import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DatenschutzShortPage } from './datenschutz-short.page';

const routes: Routes = [
  {
    path: '',
    component: DatenschutzShortPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DatenschutzShortPage]
})
export class DatenschutzShortPageModule {}

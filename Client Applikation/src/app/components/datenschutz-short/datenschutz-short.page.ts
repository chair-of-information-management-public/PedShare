import { Component, OnInit, Input } from '@angular/core';
import {  ModalController } from '@ionic/angular';

@Component({
  selector: 'app-datenschutz-short',
  templateUrl: './datenschutz-short.page.html',
  styleUrls: ['./datenschutz-short.page.scss'],
})
export class DatenschutzShortPage implements OnInit {

  @Input() DSGVOCheck: any;

  constructor(private modalController: ModalController) { }

  ngOnInit() {
  }
  dismissModal() {
    this.modalController.dismiss();
  }
  agreeToDSGVO(){
    this.DSGVOCheck = true;
    
  }
}

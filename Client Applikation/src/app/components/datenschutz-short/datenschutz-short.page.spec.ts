import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatenschutzShortPage } from './datenschutz-short.page';

describe('DatenschutzShortPage', () => {
  let component: DatenschutzShortPage;
  let fixture: ComponentFixture<DatenschutzShortPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatenschutzShortPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatenschutzShortPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

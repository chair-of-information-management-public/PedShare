import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { CloudService } from 'src/app/services/cloud.service';
import { DatabaseService } from 'src/app/services/database.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-update-booking',
  templateUrl: './update-booking.page.html',
  styleUrls: ['./update-booking.page.scss'],
})
export class UpdateBookingPage implements OnInit {

    // Parameter übergeben von Bookings
    @Input() booking: any;
    @Input() user: any;

    // UserInputs
    endStation:any;
    startTime:string;
    endTime:string;

    //UI Controls
    allStations = [];
    bookingPossible = {};
    isBikeAvailable = true;
    now = moment().toISOString(true);
    inOneYear = moment().add(1, "year").toISOString(true);

  constructor(private modalController: ModalController, private toastService: ToastService, private databaseService:DatabaseService,
     private cloudService: CloudService) {
  }

  ngOnInit() {

    this.databaseService.getBicycleStations(this.user.companyId).then(stations => {
      this.allStations = stations;
      this.endStation = this.allStations.filter(s => (s.id == this.booking.endstationId))[0];
  });
  this.startTime = moment(this.booking.startdate).toISOString(true);
  this.endTime = moment(this.booking.enddate).toISOString(true);
  }

  async updatePossible() {
    await this.toastService.presentLoading('Wir prüfen ob die Veränderung der Buchung möglich ist...').then(request=>{
        this.cloudService.isUpdatePossible(this.booking.startstationId,this.endStation.id, moment(this.startTime).format('DD.MM.YYYY HH:mm'),
        moment(this.endTime).format('DD.MM.YYYY HH:mm'),this.booking.bicycleId,this.booking.id).then(response => {
          this.toastService.dismissLoader();
          if (response['status'] == "success"){
            this.isBikeAvailable = response['isBikeAvailable'];
          }
          else{
            this.toastService.presentToast('Ein unerwarteter Fehler ist aufgetreten. Später erneut versuchen.', "warning");
          }  
      })
    })
  }

  async updateBooking(){
    this.booking.startdate = moment(this.startTime);
    this.booking.enddate = moment(this.endTime);
    this.booking.endstationId = this.endStation.id;
    this.booking.endstationName = this.endStation.name;

    await this.toastService.presentLoading('Die Buchung wird angepasst...').then(request=>{
      this.cloudService.updateBooking(this.booking.startstationId,this.endStation.id, moment(this.startTime).format('DD.MM.YYYY HH:mm'),
      moment(this.endTime).format('DD.MM.YYYY HH:mm'),this.booking.bicycleId,this.booking.id,this.endStation.name).then(response => {
        this.toastService.dismissLoader();
        if(response["status"] == "success"){
          this.isBikeAvailable = response['bookingPossible']
          if(this.isBikeAvailable){
            this.dismissModal(true);
            this.toastService.presentToast('Die Buchung wurde erfolgreich angepasst', "success");
          }
          else{
            this.toastService.presentToast('Die Anpassung war leider nicht möglich. Vermutlich wurde das Rad zwischenzeitlich gebucht.', "warning");
          }
        }
        else {
          this.toastService.presentToast('Es ist ein unbekannter Fehler aufgetreten. Probiere es später erneut.', "warning");
        }
      });
    });
  }

autoSetDropOffTime(){
  this.endTime = moment(this.startTime).add(2, 'hours').toISOString(true);
}

// Schließt das Modal
dismissModal(submitted = false) {
  this.modalController.dismiss({
    'dismissed': true,
    submitted: submitted,
    booking: this.booking
  });
}
}

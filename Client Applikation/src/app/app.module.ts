import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { GoogleMaps } from '@ionic-native/google-maps';

import { AngularFireModule } from '@angular/fire/';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { firebaseConfig } from './credentials';
import { Geolocation } from '@ionic-native/geolocation/ngx';

import { MakeBookingModalPageModule } from './components/make-booking-modal/make-booking-modal.module';

import { DatenschutzPageModule } from './components/datenschutz/datenschutz.module';

import { DatenschutzShortPageModule } from './components/datenschutz-short/datenschutz-short.module';

import { HttpClientModule } from '@angular/common/http';

import { Network } from '@ionic-native/network/ngx';
import { AngularFireAuthGuard } from '@angular/fire/auth-guard';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { BLE } from '@ionic-native/ble/ngx';
import { BookingsPageModule } from './pages/bookings/bookings.module';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import {DynSurveyModalPageModule} from "./components/dyn-survey-modal/dyn-survey-modal.module"

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, AngularFirestoreModule,
    AngularFireModule.initializeApp(firebaseConfig), MakeBookingModalPageModule, DatenschutzPageModule, DatenschutzShortPageModule, HttpClientModule, AngularFireAuthModule, DynSurveyModalPageModule
  ],
  providers: [
    StatusBar, BLE,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    GoogleMaps,
    Geolocation,
    Network,
    AngularFireAuthGuard, LocalNotifications, Diagnostic,
    EmailComposer,
    CallNumber,
    InAppBrowser
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

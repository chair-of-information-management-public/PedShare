
## Required, hardcoded credentials 
marked with placeholder '*% insert here %*':
* Firebase Credentials in *src/app/credentials.ts* 
* Firebase Function Root URL in *src/app/environments/environments.prod.ts* 
* Firebase Function Root URL in *src/app/environments/environments.ts* 
* Google Maps API Key in *src/app/pages/home.page.ts* (line 115, 116) 
* Google Maps API Key in *config.xml* (line 22, 23)
* Google Maps API Key in *package.json* (line 102, 103)
* Connection Details I-Lock-It Schlösser in *src/app/services/ilockit.service.ts*
* Firebase Connection Credentials in *google-services.json*
* Firebase Connection Credentials in *GoogleService-Info.plist*
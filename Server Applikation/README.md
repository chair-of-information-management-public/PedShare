# Readme

## Before you start

Make sure that you enter hard-coded urls in *functions/src/util.ts* (line 10, 11, 12)

## Install Firebase Project in VS Code

1. Install Visual Studio Code

    https://code.visualstudio.com/download

2. Install Postman

    https://www.postman.com/downloads/

3. Go to Gitlab into your project

4. Clone your Project

    https://code.visualstudio.com/docs/editor/github#:~:text=Cloning%20a%20repository%23,you%20have%20no%20folder%20open).
    
    + Klick Clone-Button
    + Open in your IDE
    + Visual Studio Code (HTTPS) 

5. Open VS Code

6. Open a new terminal

    https://code.visualstudio.com/docs/editor/integrated-terminal

7. Go to CloudFunctions folder
```
    cd ...\PedShare\PedShareCloudFunctions\functions
```

8. Install npm
```
    npm install
```

9. npm run build
```
    npm run build
```

10. Download the key of your project

    1. Go to: https://console.cloud.google.com/iam-admin/serviceaccounts
    2. Look at the service account: App Engine default service account
    3. Click on Actions -> Manage Key
    4. Add new key as JSON file
    5. Download the keyfile
    6. Shift the key file to a save place

11. Set environmental variable

    https://firebase.google.com/docs/functions/local-emulator?hl=en
    1. Search for "environment variables" on Windows
    2. Click on "New"
    3. The name have to be "GOOGLE_APPLICATION_CREDENTIALS"
    4. The value is the path of your key file from step 10. 


12. Install Firebase

    https://firebase.google.com/docs/cli#windows-npm

    Go back to VS Code
```
    npm install firebase
```
13. Login in Firebase
```
    firebase login 
```
14. Install Firebase Tools
```    
    npm install -g firebase-tools
```
15. Load Cloudfuntions local
```
    firebase serve
```

16. Run Debugger
```
    npm run debug
```

Other options to debug:

https://stackoverflow.com/questions/45920014/functions-debugging-in-vs-code

https://medium.com/firebase-developers/debugging-firebase-functions-in-vs-code-a1caf22db0b2

17. Test with Postman

    https://learning.postman.com/docs/getting-started/importing-and-exporting-data/
    1. Open Postman
    2. Click on File -> Import
    3. Import the file collection of the project (\PedShare\PedShareCloudFunctions\PedShare.postman_collection)
    4. For Testing in Postman your debugger in VS Code have to run (Step 16)
    5. In the collection go to Pedshare -> Firebase Cloud Functions
    6. You can see all function you can test
    7. **Attention**: You test the functions with your real database. So if you change data during the test, your database would change.


## Start Project

1. The project must be installed

2. Open VS Code

3. Open new Terminal

4. Go to CloudFunctions folder
```
    cd ...\PedShare\PedShareCloudFunctions\functions
```

5. Run Debugger (see Step 16)
```
    npm run debug
```

6. Open Postman (see Step 17)

    1. For Testing in Postman your debugger in VS Code have to run (Step 16)
    2. In the collection go to Pedshare -> Firebase Cloud Functions
    3. You can see all function you can test
    4. **Attention**: You test the functions with your real database. So if you change data during the test, your database would change.

## Hosting Firebase

Follow the instruction of: https://firebase.google.com/docs/hosting/quickstart?hl=en


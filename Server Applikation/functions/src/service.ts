import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as moment from 'moment-timezone';
import * as https from 'https';
import axios from 'axios';
import createGpx from 'gps-to-gpx' //).default;
moment.tz.setDefault("Europe/Berlin");
import * as nodeutil from 'util';
import * as util from './util';


// Returns mail-adresses from all registered pedshare users as mailto link
export const getAllMail = functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    admin.firestore().collection('users').where("email", ">=", "").get() 
    .then(userSnapshots => {
        let mailtoLink="";
        userSnapshots.forEach(user => {
            mailtoLink+= user.data().email + "; ";                          
        });
                
        response.status(200).send({success: true, mailtoLink: mailtoLink });
        }).catch(err => {
            return response.send({success: false, error: nodeutil.inspect(err)})
        })
});

// Adds new bike to backend
export const addBikeToBackend = functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.body;
    if(!data.companyId || !data.currentStationId || !data.name || !data.lockname || !data.lockmac || !data.locktraccarId || !data.locktoken ) {
        return response.send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }

    admin.firestore().collection('bicycles').doc().set({
        companyId: data.companyId,
        currentStationId: data.currentStationId,
        name: data.name,
        lock: {
            mac: data.lockmac,
            name: data.lockname,
            token: data.locktoken,
            traccarId: data.locktraccarId,
        }}).then( () => {return response.send({success: true}); 
        }).catch(err => {return response.send({success: false, error: nodeutil.inspect(err)})})   
});

export const addCompaniesToBackend = functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.query;
    if(!data.domains || !data.latitude || !data.longitude || !data.mobilityManagerMail || !data.mobilityManagerName || !data.mobilityManagerPhone || !data.name ) {
        return response.send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }
    /*
    admin.firestore().collection('companies').doc().set({
        domains: data.domains,
        latitude: data.latitude,
        longitude: data.longitude,
        mobilityManagerMail: data.mobilityManagerMail,
        mobilityManagerName: data.mobilityManagerName,
        mobilityManagerPhone: data.mobilityManagerPhone,
        name: data.name

        }).then( () => {return response.send({success: true}); 
        }).catch(err => {return response.send({success: false, error: nodeutil.inspect(err)})}) 
    */
   console.log("Success");  
   return 0;
});

export const addBicycleStationsToBackend = functions.region('europe-west1').https.onRequest(async(request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.query;
    if(!data.companyId || !data.latitude || !data.longitude  || !data.name ) {
        console.log(data);
        return response.send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }
   
    /*admin.firestore().collection('bicycleStations').doc().set({
        companyId: data.companyId,
        latitude: data.latitude,
        longitude: data.longitude,
        name: data.name,
        

        }).then( () => {return response.send({success: true}); 
        }).catch(err => {return response.send({success: false, error: nodeutil.inspect(err)})})   
    */
   console.log("Success");
   return 0;
    });

export const addConfigToBackend = functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.query;
    if(!data.from || !data.host || !data.pass || !data.port || !data.secure || !data.user || !data.accarAddress) {
        return response.send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }
    /*
    admin.firestore().collection('config').doc('mail').set({
        from: data.from,
        host: data.host,
        pass: data.pass,
        port: data.port,
        secure: data.secure,
        user: data.user,
        accarAddress: data.accarAddress

        }).then( () => {return response.send({success: true}); 
        }).catch(err => {return response.send({success: false, error: nodeutil.inspect(err)})})
    */
   console.log("Success"); 
   return 0;  
});


// Resynchronizes booking distances from traccar service for all bookings in db; Use preview paramter to simulate function.
export const updateBookingDistance = functions.region('europe-west1').https.onRequest(async (request:any, response:any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.query; 

    if(!data.preview) {
        response.status(200).send({success: false, msg: "Nicht genug oder fehlerhafte Daten übergeben!"})
        return;
    }
    
    try{
        const tracarByBikeId = {};
        const posByStationId = {};
        const bicycleSnapshots = await admin.firestore().collection('bicycles').get();
        bicycleSnapshots.forEach(bike => {
            tracarByBikeId[bike.id] = bike.data().lock.traccarId;
        })

        const stationSnapshots = await admin.firestore().collection('bicycleStations').get();
        stationSnapshots.forEach(station => {
            posByStationId[station.id] = {latitude: parseFloat(station.data().latitude), longitude: parseFloat(station.data().longitude)}
        })
        
        //#1. Get Bookings
        const bookingSnapshots =  await admin.firestore().collection('bookings').where('status', '==', 'finished').where('virtualBooking', '==', false).orderBy('startdate').get();
        for (const bookingSnapshot of bookingSnapshots.docs)
        {
            try{
                const booking = await util.unpackBooking(bookingSnapshot);
            
                // #2. calculcate trip distance
                let trip = await util.calculateTripDistance(booking, tracarByBikeId[booking.bicycleId]);

                const pedShareDistance = booking.totalDistanceInM;                
                const gpsDistance = trip.totalDistanceInM;

                // #3 UDPATE BOOKING
                if (gpsDistance > pedShareDistance){
                    console.log("Buchung von " + booking.startdate.format('DD.MM.YYYY HH:mm') + " bis " + booking.enddate.format('DD.MM.YYYY HH:mm') + " [" + booking.id + "]");
                    console.log("PedShare Distanz: " + (pedShareDistance/1000.0).toFixed(2));
                    console.log("GPS-Berechnet Distanz: " + (gpsDistance/1000.0).toFixed(2));
                    if (data.preview.toLowerCase() === "false"){
                        await booking.ref.update(
                            {
                                totalDistanceInM: gpsDistance,
                                totalAvgSpeedInKMH: (gpsDistance/1000.0) / (booking.totalDurationInM/60.0),
                                totalco2SavingsInKg: (gpsDistance / 1000) *  0.21516
                            }
                        );
                        console.log("UPDATE SUCCESS");
                    }
                    console.log("###########################");
                }
            }
            catch(ex){
                console.log("Error when processing #" + bookingSnapshot.id);
                console.log("###########################")
            }  
        }

        return response.send({success: true});
    }
    catch(ex) {
        return response.send({success: false, error: nodeutil.inspect(ex)})
    }        
});

// Resynchronizes user achivements from all finished bookings in db; 
// Use preview parameter to simulate function.
export const updateUserAchievements = functions.region('europe-west1').https.onRequest(async (request:any, response:any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.query; 

    if(!data.preview) {
        return response.send({success: false, msg: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }
    
    try{
        const userSnapshots = await admin.firestore().collection('users').get();
        for (const userSnapshot of userSnapshots.docs) {
            const user = util.unpackUser(userSnapshot)
            // RANKING
            let ranking = {
                totalco2SavingsInKg: 0,
                totalDistanceInM: 0,
                monthlyco2SavingsInKg: 0,
                monthlyDistanceInM: 0,
            }

            // ACHIEVEMENTS
            let achievements = {
                totalNumberOfBookings: 0,
                totalActiveMonths: 0,
                totalDurationInM: 0,
                totalco2SavingsInKg: 0,
                totalDistanceInM: 0,
                monthlyDurationInM: 0,
                monthlyco2SavingsInKg: 0,
                monthlyDistanceInM: 0,
                monthlyNumberOfBookings: 0
            }

            let activeMonths:any = [];
            
            const bookingSnapshots = await admin.firestore().collection('bookings').where("uid", "==", user.uid).where('status', '==', 'finished').where('virtualBooking', '==', false).get()                     
            bookingSnapshots.forEach(bookingSnapshot => {
                const booking = util.unpackBooking(bookingSnapshot);
                    achievements.totalNumberOfBookings+=1
                    activeMonths.push(booking.startdate.format('MM.YYYY'));
                    achievements.totalDurationInM+=booking.totalDurationInM;
                    achievements.totalDistanceInM+=booking.totalDistanceInM;
                    if(booking.startdate.isSame(new Date(), "month")){
                        achievements.monthlyDurationInM+=booking.totalDurationInM;
                        achievements.monthlyDistanceInM+=booking.totalDistanceInM;
                        achievements.monthlyNumberOfBookings+=1;
                    }

                    if(booking.enabledRanking){
                        ranking.totalDistanceInM+=booking.totalDistanceInM;
                        if(booking.startdate.isSame(new Date(), "month")){
                            ranking.monthlyDistanceInM+=booking.totalDistanceInM;
                        }
                    }
            });

            // CO2 Values
            ranking.totalco2SavingsInKg = (ranking.totalDistanceInM / 1000) *  0.21516;
            ranking.monthlyco2SavingsInKg = (ranking.monthlyDistanceInM / 1000) *  0.21516;
            achievements.totalco2SavingsInKg = (achievements.totalDistanceInM / 1000) *  0.21516;
            achievements.monthlyco2SavingsInKg = (achievements.monthlyDistanceInM / 1000) *  0.21516;
            achievements.totalActiveMonths = new Set(activeMonths).size;
            
            console.log("User #" + user.uid);
            console.log("- Ranking -");
            console.log("Distance: " + user.totalDistanceInM + " -> " + ranking.totalDistanceInM);
            console.log("Distance Monthly: " + user.monthlyDistanceInM + " -> " + ranking.monthlyDistanceInM);
            console.log("- Achievements -");
            console.log("Bookings: " + user.achievements.totalNumberOfBookings + " -> " + achievements.totalNumberOfBookings);
            console.log("Active Months: " + user.achievements.totalActiveMonths + " -> " + achievements.totalActiveMonths);
            console.log("Bookings Monthly: " + user.achievements.monthlyNumberOfBookings + " -> " + achievements.monthlyNumberOfBookings);
            console.log("Distance: " + user.achievements.totalDistanceInM + " -> " + achievements.totalDistanceInM);
            console.log("Distance Monthly: " + user.achievements.monthlyDistanceInM + " -> " + achievements.monthlyDistanceInM);
            console.log("Duration: " + user.achievements.totalDurationInM + " -> " + achievements.totalDurationInM);
            console.log("Duration Monthly: " + user.achievements.monthlyDurationInM + " -> " + achievements.monthlyDurationInM);               
        
            if (data.preview.toLowerCase() === "false"){
                await user.ref.update({
                    totalco2SavingsInKg: ranking.totalco2SavingsInKg,
                    totalDistanceInM: ranking.totalDistanceInM,
                    monthlyco2SavingsInKg: ranking.monthlyco2SavingsInKg,
                    monthlyDistanceInM: ranking.monthlyDistanceInM,
                    achievements
                })
                console.log("UPDATE SUCCESS");
            }
            console.log("#################################")
        };
        return response.send({ success: true});
    }
    catch(ex) {
        return response.send({success: false, error: nodeutil.inspect(ex)})
    }        
});

// Resynchronizes company ranking from all finished bookings in db; 
// Also updates medals for users; 
// MAKE SURE THAT USERS ARE UP TO DATE before executing function; 
// Use preview parameter to simulate function.
export const updateCompanyAchievements = functions.region('europe-west1').https.onRequest(async (request:any, response:any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.query; 

    if(!data.preview) {
        response.status(200).send({success: false, msg: "Nicht genug oder fehlerhafte Daten übergeben!"})
        return;
    }
    
    try{
        const companySnapshots = await admin.firestore().collection('companies').get();

        for(const companySnapshot of companySnapshots.docs){
            // ACHIEVEMENTS
            let achievements = {
                totalNumberOfBookings: 0,
                totalDurationInM: 0,
                totalco2SavingsInKg: 0,
                totalDistanceInM: 0,
                totalNumberOfUsers: 0,
                totalNumberOfBikes: 0,
                totalNumberOfStations: 0
            }

            const userSnapshots = await admin.firestore().collection('users').where('companyId', '==', companySnapshot.id).get();
            const bikeSnapshots = await admin.firestore().collection('bicycles').where('companyId', '==', companySnapshot.id).get();
            const stationSnapshots = await admin.firestore().collection('bicycleStations').where('companyId', '==', companySnapshot.id).get();
            const bookingSnapshots = await admin.firestore().collection('bookings').where('virtualBooking', '==', false).get();

            for(const bookingSnapshot of bookingSnapshots.docs){
                //if user part of company -> add booking
                if(userSnapshots.docs.filter(u => u.id === bookingSnapshot.data().uid).length > 0){
                    achievements.totalNumberOfBookings += 1;
                }
            }
            
            achievements.totalNumberOfUsers = userSnapshots.size;
            achievements.totalNumberOfBikes = bikeSnapshots.size;
            achievements.totalNumberOfStations = stationSnapshots.size;

            for (const userSnapshot of userSnapshots.docs) {
                const user = util.unpackUser(userSnapshot);
                achievements.totalDistanceInM += user.achievements.totalDistanceInM;
                achievements.totalDurationInM += user.achievements.totalDurationInM;
                achievements.totalco2SavingsInKg += user.achievements.totalco2SavingsInKg;
            }

            console.log("Company #" + companySnapshot.id + " (" + companySnapshot.data().name + ")");
            console.log("- Achievements -");
            console.log("Bookings: " + achievements.totalNumberOfBookings);
            console.log("Bikes: " + achievements.totalNumberOfBikes);
            console.log("Stations: " + achievements.totalNumberOfStations);
            console.log("Total Distance: " + achievements.totalDistanceInM);
            console.log("Total Duration: " + achievements.totalDurationInM);

            if (data.preview.toLowerCase() === "false"){
                await companySnapshot.ref.update({
                    achievements
                })
                console.log("UPDATE SUCCESS");
            }
            console.log("#################################")
        };
        return response.send({ success: true});
    }
    catch(ex) {
        return response.send({success: false, errr: nodeutil.inspect(ex)})
    }        
});

// Returns report on platform usage containing KPIs for given time-frame and all-time
export let generateUsageReport = functions.region('europe-west1').https.onRequest(async (request:any, response:any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');

    const data = request.query;
    if(!data.startDate || !data.endDate) {
        return response.send({success: false, message: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }
            
    try{
        let allTimeReport = {}
        let filteredReport = {}
        const startDate = moment(data.startDate, 'DD.MM.YYYY').toDate()
        const endDate = moment(data.endDate, 'DD.MM.YYYY').toDate()

        // Total Number Registered Users
        const userSnapshots =  await admin.firestore().collection('users').get();
        allTimeReport["total_number_of_users"] = userSnapshots.size;
        // Total Number of Ranking Participants
        let total_ranking_participants_count = 0
        userSnapshots.forEach(userSn => {
            const userRanking = userSn.data();
            if(userRanking.enableRanking){
                total_ranking_participants_count+=1;
            }
        });
        allTimeReport["total_ranking_participants_count"] = total_ranking_participants_count;
        
        // Number of Bookings - filterd on query
        const bookingSnapshots =  await admin.firestore().collection('bookings').get();
        allTimeReport["total_number_of_bookings"] = bookingSnapshots.size;

        let total_distance_driven_M = 0;
        let total_time_driven_M = 0;
        
        let booking_count = 0;
        let distance_driven_M = 0;
        let time_driven_M = 0;


        bookingSnapshots.forEach(bookingSn => {
            const booking = bookingSn.data();
            booking.startdate = moment(booking.startdate.toDate());
            booking.enddate = moment(booking.enddate.toDate());
            
            if(booking.totalDistanceInM)
                total_distance_driven_M+=booking.totalDistanceInM;
            
            if(booking.totalDurationInM)
                total_time_driven_M += booking.totalDurationInM;
            
            // check whether booking is in query timespan
            if (booking.startdate > startDate && booking.startdate < endDate)
            {
                booking_count+=1;
                if(booking.totalDistanceInM)
                    distance_driven_M += booking.totalDistanceInM;
                if(booking.totalDurationInM)
                    time_driven_M += booking.totalDurationInM;
            } 
        });

        allTimeReport["total_distance_driven_M"] = total_distance_driven_M;
        allTimeReport["total_time_driven_M"] = total_time_driven_M;
        allTimeReport["total_co2_savings_kg"] =  (total_distance_driven_M / 1000) *  0.21516;
        

        filteredReport["booking_count"] = booking_count;
        filteredReport["distance_driven_M"] = distance_driven_M;
        filteredReport["time_driven_M"] = time_driven_M;
        filteredReport["co2_savings_kg"] =  (distance_driven_M / 1000) *  0.21516;
        
        // User with most bookings

        // User with most distance


        return response.send({success: true, allTimeReport: allTimeReport, filteredReport: filteredReport});
    }
    catch(ex) {
        return response.send({success: false, error: nodeutil.inspect(ex)})
    }        
});

// Returns GPX Track to Booking ID
export let getGpxTrack = functions.region('europe-west1').https.onRequest(async (request:any, response:any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.query;

    if(!data.bookingId) {
        return response.send({success: false, message: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }
    
    try{
        const booking = await util.getBooking(data.bookingId);
        const bike = await util.getBicycle(booking.bicycleId);
        const traccarId = bike.lock.traccarId;
        
        const route = await axios({
            method: 'get',
            url: `https://${util.traccarIp}/api/reports/route?deviceId=${traccarId}&from=${booking.startdate.toISOString()}&to=${booking.enddate.toISOString()}`, 
            headers: { 'Authorization': util.traccarAuth},  
            httpsAgent: new https.Agent({ rejectUnauthorized: false })
            });
        
        const routeSummary = route.data;

        const positions:Array<any> = [];
        
        for(const position of routeSummary){
            positions.push({
                latitude: position["latitude"],
                longitude: position["longitude"],
                time: moment(position["fixTime"]).toISOString(),
            });
        }

        const gpx = createGpx(positions); 
        // fs.writeFile(id + ".gpx", gpx, function (err) {
        //     if (err) console.log("File ERROR: " + err);
        //     }); 

        return response.send(gpx);
    }
    catch(ex) {
        return response.send({success: false, error: nodeutil.inspect(ex)})
    }   
});

// Returns all submitted responses to a given questionaire (id) in csv format
export const getCsvOfSurvey = functions.region('europe-west1').https.onRequest(async (request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');

    const data = request.query;  

    if(!data.id) {
        response.status(200).send({success: false, msg: "Nicht genug oder fehlerhafte Daten übergeben!"})
        return;
    }

    try{
        const surveySnapshot = await admin.firestore().collection('surveys').doc(data.id).get();
        const submissionSnapshots = await admin.firestore().collection('surveySubmissions').where("surveyId","==",data.id).get();
   
        let survey = surveySnapshot.data();
        let question:any = [];
        
        if(survey){
            survey.questions.forEach(t => {
                question.push(t.text + ";");
            });
            
            let result:any = [];
            result.push(question);

            let answers:any =[ ];
            submissionSnapshots.forEach(answer => {                              
                answers.push(answer.data().answers);                
            });
            
            answers.forEach(rows => {
                let row = String(Object.keys(rows).map(function(key) {return rows[key];}));                
                result.push("Antwort:;"+row.replace(/,/gi,";"));
            });        
            
            let csvContent = "Fragen:;";
            
            result.forEach(rowArray => {
                csvContent += rowArray + "\r\n";
            });
            
            return response.send(csvContent);
        }
        else{
            throw(Error("Could not find survey #" + data.id));
        }

    }
    catch(ex){
        return response.send({success: false, error: nodeutil.inspect(ex)});
    }
});

// Returns all bookings as json representation
export const getMetaOfBookings = functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
            
    admin.firestore().collection('bookings').get().then(bookingSnapshots => {
        let bookings:any=[];
        bookingSnapshots.forEach(bookingSnapshot => {
            bookings.push(util.unpackBooking(bookingSnapshot, false, true));
        });
                            
        response.status(200).send(bookings);
        }).catch(err => {
            return response.send({success: false, error: nodeutil.inspect(err)})
        })
});

// Returns all companies as json representation
export let getMetaOfCompanies= functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
            
    admin.firestore().collection('companies').get().then(companySnapshots => {
        let companies:any=[];
        companySnapshots.forEach(companySnapshot => {
            let company = {
                id: companySnapshot.id,
                name: companySnapshot.data().name
            }
            companies.push(company);
        });
                            
        response.status(200).send(companies);
        }).catch(err => {
            return response.send({success: false, error: nodeutil.inspect(err)})
        })
});
// Returns all bike stations as json representation
export const getMetaOfStations = functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
            
    admin.firestore().collection('bicycleStations').get().then(bycycleStationSnapshots => {
        let stations:any=[];
        bycycleStationSnapshots.forEach(bycycleStationSnapshot => {
            let bicycleStation = {
                id: bycycleStationSnapshot.id,
                company: bycycleStationSnapshot.data().companyId
            }
            stations.push(bicycleStation);
        });
                            
        response.status(200).send(stations);
        }).catch(err => {
            return response.send({success: false, error: nodeutil.inspect(err)})
        })
});

/*export let getArchievmentOfUser = functions.region('europe-west1').https.onRequest(async (request: any, response: any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
        const data = request.query; 

        if(!data.uid) {
            response.send({success: false, message: "Nicht genug oder fehlerhafte Daten übergeben!"});
        }
        
        const user = await util.getUser(data.uid);
              
        let userReport= await util.getDistanceReport(data.uid, false);
       
        admin.firestore().collection('users').where("companyId", "==", user.companyId).get() 
        .then(async userSnapshot => { 
            if(user.enableRanking){  

                let reportOfAllUser : any[]=[];            
                const listOfAllUserOfCompany : any[]=[];

                userSnapshot.forEach(userOfCompany => {listOfAllUserOfCompany.push(userOfCompany.data().uid)});
                for (const users of listOfAllUserOfCompany){            
                    reportOfAllUser.push(await util.getDistanceReport(users, true));
                }  
                //sort by totalDistanceInM 
                reportOfAllUser.sort((a, b)=> {
                    return a.totalDistanceInM - b.totalDistanceInM;
                });
                for(let i=1;i<reportOfAllUser.length;i++){
                    if(reportOfAllUser[i].totalDistanceInM>userReport.totalDistanceInM){
                        userReport.rankingTotalDistance=reportOfAllUser.length-i+1;
                        break;
                    }
                    if(i===reportOfAllUser.length-1){
                        userReport.rankingTotalDistance=1;
                    }
                }
                //sort by totalDurationInM 
                reportOfAllUser.sort((a, b)=> {
                    return a.totalDurationInM - b.totalDurationInM;
                });
                for(let i=1;i<reportOfAllUser.length;i++){                    
                    if(reportOfAllUser[i].totalDurationInM>userReport.totalDurationInM){
                        userReport.rankingTotalDuration=reportOfAllUser.length-i+1;                        
                        break;
                    }
                    if(i===reportOfAllUser.length-1){
                        userReport.rankingTotalDuration=1;
                    }
                }

                reportOfAllUser.sort((a, b)=> {
                    return a.monthlyDistanceInM - b.monthlyDistanceInM;
                });
                for(let i=1;i<reportOfAllUser.length;i++){
                    if(reportOfAllUser[i].monthlyDistanceInM>userReport.monthlyDistanceInM){
                        userReport.rankingMonthlyDistanceInM=reportOfAllUser.length-i+1;
                        break;
                    }
                    if(i===reportOfAllUser.length-1){
                        userReport.rankingMonthlyDistance=1;
                    }
                }

                reportOfAllUser.sort((a, b)=> {
                    return a.monthlyDurationInM - b.monthlyDurationInM;
                });
                for(let i=1;i<reportOfAllUser.length;i++){
                    if(reportOfAllUser[i].monthlyDurationInM>userReport.monthlyDurationInM){
                        userReport.rankingMonthlyDurationInM=reportOfAllUser.length-i+1;
                        break;
                    }
                    if(i===reportOfAllUser.length-1){
                        userReport.rankingMonthlyDuration=1;
                    }
                }  
            }
                     
            response.status(200).send({success: true, Test: userReport });
            }).catch(err => {
                response.send({success: false, error: err.msg})
            })
                
})
*/
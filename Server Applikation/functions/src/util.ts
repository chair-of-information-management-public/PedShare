import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as moment from 'moment-timezone';
import * as https from 'https'
import axios from 'axios';
import * as nodemailer from 'nodemailer'
import * as haversine from 'haversine'

moment.tz.setDefault("Europe/Berlin");
export const traccarIp = "% insert here %";
export const traccarAuth = "% insert here %";
export const firebaseUrl = "% insert here %";


export function isBikeAvailable(startStation, endStation, start, end, bicycleId, bookingId:string='') {
    return Promise.all([
        isBikeAvailableForDropOff(end, endStation, bicycleId, bookingId),
        isBikeAvailableForPickup(start, startStation, bicycleId, bookingId),
        isBikeAvailableInTimespan(start, end,bicycleId, bookingId)
    ]).then(conditionFullfilled => {
        return !conditionFullfilled.includes(false);
    });
}

function isBikeAvailableForDropOff(end, endStation, bicycleId, bookingId:string = ''){
    // A bike is available to dropoff at a station B, when there is no future booking or a future booking which starts at station B
    return admin.firestore().collection('bookings').where("bicycleId", "==", bicycleId).
        where('startdate', ">=", end).
        orderBy('startdate').
        limit(1).
        get().
        then(bookings => {
            let dropOffWorks = true;
            if(bookings.empty)//No future booking
                return true;
            else
            {        
                bookings.forEach(booking => {
                    if(booking.id !== bookingId)
                        if(booking.data().startstationId !== endStation)//next booking starts somewhere else 
                            dropOffWorks = false
                });
            }
            return dropOffWorks;
        });
}

function isBikeAvailableForPickup(start, startStation, bicycleId, bookingId:string = ''){
    // A bike is available for pickup at a station A, when the previous booking ended at the station A
    return admin.firestore().collection('bookings').where("bicycleId", "==", bicycleId).
        where('enddate', "<=", start).
        orderBy('enddate', 'desc').
        limit(1).
        get().
        then(async bookings => {
            let pickupWorks = true;
            if(bookings.empty){ //no previous booking -> check currentstation of bike
                const bicycle = await getBicycle(bicycleId);
                return (bicycle.currentStationId === startStation)
            }   
            else
            {
                bookings.forEach(booking => {
                    if(booking.id !== bookingId)
                        if(booking.data().endstationId !== startStation)//previous booking ended somewhere else
                            pickupWorks = false
                });
            }
            return pickupWorks
        });
}

function isBikeAvailableInTimespan(start, end, bicycleId, bookingId:string = ''){
    // A bike is available when there are only bookings before and after a given timeframe 
    // -> This means if (StartBooking <= End)) and ((EndBooking >= Start)  there is an overlap, which means the bike is not available
    return admin.firestore().collection('bookings').where("bicycleId", "==", bicycleId).
        where('startdate', "<", end).
        get().
        then(bookings => {
            // 1. Overlap Test
            let overlapping = false;
            bookings.forEach(booking => {
            if(booking.id !== bookingId){ 
                if(booking.data().enddate.toDate() > start)
                    {
                        functions.logger.info("Overlapping booking detected: #" + booking.id);
                        overlapping = true;  
                    }
                }
            })
            return !overlapping; 
        });
}

export function getBooking(bookingId){
    return admin.firestore().collection('bookings').doc(bookingId).get().then(bookingSnapshot => {
        return unpackBooking(bookingSnapshot, true);       
    });
}

export function unpackBooking(bookingSnapshot, includeRef = true, removePersonalData = false){
    const id = bookingSnapshot.id;
    let booking = bookingSnapshot.data();
    if(includeRef){
        booking.ref = bookingSnapshot.ref; 
    }
    if(removePersonalData) 
    {
        if(booking.startstationName){
                delete booking.startstationName;
                delete booking.endstationName;
                delete booking.start_lat;
                delete booking.start_lng;
                delete booking.end_lat;
                delete booking.end_lat;
            }
    }
    
    booking.id = id;
    booking.startdate = moment(booking.startdate.toDate());
    booking.enddate = moment(booking.enddate.toDate());

    // Fill potential empty values 
    booking.totalco2SavingsInKg = (booking.totalco2SavingsInKg)? booking.totalco2SavingsInKg: 0;
    booking.totalDistanceInM = (booking.totalDistanceInM)? booking.totalDistanceInM: 0;
    return booking;
}

export function getBicycle(bikeId){
    return admin.firestore().collection('bicycles').doc(bikeId).get().then(bikeSnapshot => {
        const bike = bikeSnapshot.data();
        if(bike){
            bike.ref = bikeSnapshot.ref; 
            bike.id = bikeSnapshot.id;
            return bike;
        }
        else{
            throw(Error("Bicycle #" + bikeId + " couldn't be found."))
        } 
    });
}

export function getUser(uid){
    return admin.firestore().collection('users').doc(uid).get().then(userSnapshot => {
       return unpackUser(userSnapshot); 
    });
}

export function unpackUser(userSnapshot){
    const user = userSnapshot.data();
        user.ref = userSnapshot.ref; 
        //Make sure that missing values are set correctly
        user.totalco2SavingsInKg = (user.totalco2SavingsInKg)? user.totalco2SavingsInKg: 0;
        user.totalDistanceInM = ( user.totalDistanceInM)? user.totalDistanceInM: 0; 
        user.monthlyco2SavingsInKg = ( user.monthlyco2SavingsInKg)? user.monthlyco2SavingsInKg: 0;
        user.monthlyDistanceInM = (user.monthlyDistanceInM)? user.monthlyDistanceInM : 0;
        user.totalMonthlyWins =  (user.totalMonthlyWins)? user.totalMonthlyWins: 0;
        user.achievements = (user.achievements)? user.achievements: {totalNumberOfBookings: 0, totalActiveMonths: 0, totalDurationInM: 0, totalco2SavingsInKg: 0, totalDistanceInM: 0, monthlyDurationInM: 0, monthlyco2SavingsInKg: 0, monthlyDistanceInM: 0, monthlyNumberOfBookings: 0 }
        return user;
}

export function attachTrip(booking, enddate){
    //return admin.firestore().collection('bicycles').doc(booking.bicycleId).get().then(bicycle => {
        //attach trip to firestore
        return admin.firestore().collection('bookings').doc(booking.id).collection('trips')
            .orderBy('start', 'desc').limit(1).get()
            .then(async tripSnapshots =>  {
                const trip = tripSnapshots.docs[0];
                const tripDurationInM = enddate.diff(moment(trip.data().start.toDate()), 'minutes');
                const totalDurationInM = booking.totalDurationInM + tripDurationInM;
                
                return Promise.all([
                    trip.ref.update({
                        tripDurationInM,
                        end: enddate.toDate()
                    }),
                    booking.ref.update({
                        totalDurationInM
                    })
                ]);    
            })      
}

export function isBookingValid(startStation, endStation, start, end, bicycleId, userId, bookingId:string=''){
    return Promise.all([
        isBikeAvailable(startStation, endStation, start, end, bicycleId, bookingId),
        isUserAbleToBook(start, end, userId, bookingId)
    ]).then(conditionFullfilled => {
        return !conditionFullfilled.includes(false);
    });
}

//todo insert in update
export function isUserAbleToBook(start, end, userId, bookingId:string=''){
    //A user is able to book a bike, when there is no ongoing booking in parallel
    return admin.firestore().collection('bookings').where("uid", "==", userId).
    where('startdate', "<", end).
    get().
    then(bookings => {
        // Overlap Test
        let overlapping = false;
        bookings.forEach(booking => {
            if(booking.id !== bookingId){ 
                if(booking.data().enddate.toDate() > start)
                {
                    functions.logger.info("Overlapping booking detected: #" + booking.id);
                    overlapping = true;
                }
            }
        })
        return !overlapping;
    });
}

export function generateFunFact(distanceInM, durationInM, co2savingsInKg, id=-1){
    const co2savingsInKgRound = Math.round(co2savingsInKg*100)/100;
    
    
    let randId;
    //ANZAHL FUNFACTS __ Muss geändert werden wenn mehr hinzugefügt wurden
        // + 1 da sonst 0 und der maximale Wert nicht vorkommen können

        //ID um Funfacts direkt ansprechen zu können (u.a. zum testen)
        if(id===-1){
            randId = String(Math.floor(Math.random() * 12)+1);
        }
        else{
            randId = String(id);
        }
        return admin.firestore().collection('funfacts').where('id', '==', randId).
            get().
            then(funfacts => {
                let generatedFunFactText;
                if (funfacts.empty) {
                   generatedFunFactText = "Leider konnte kein passender Funfact gefunden werden";
                }
                else {
                     funfacts.forEach(funfactFetched => {
                        const funfact = funfactFetched.data();
                        generatedFunFactText = funfact.text;
                        let randNo; 
                        
                        //TYP Co2-Verbrauch
                        if (funfact.type === "co2") {
                               
                            if(funfact.reference){
                                randNo= Math.floor(Math.random() * funfact.reference.length);
                             }
                            const co2Text = `${co2savingsInKgRound} kg`;
                            generatedFunFactText = generatedFunFactText.replace('[placeholderCo2Savings]', co2Text);
                            
                            //berechnung des zweiten Placeholders. --> Wenn vorhanden
                            if(funfact.text.includes('[comparisonObject]')){
                                generatedFunFactText = generatedFunFactText.replace('[comparisonObject]', funfact.reference[randNo]);
                                }

                            //berechnung des dritten Placeholders. --> Wenn vorhanden
                            if (funfact.text.includes('[comparisonValue]')) {
                        
                                let comparisonValueText;
                                let comparisonValue = co2savingsInKgRound*1000 / parseFloat(funfact.co2[randNo]) * parseFloat(funfact.factor[randNo]);
                                comparisonValue = Math.round(comparisonValue);
                         
                                //Umwandlung g in KG wenn Vergleichswert > = 1000g
                                if(Number(comparisonValue)>=Number(1000)){
                                    comparisonValue = comparisonValue/1000;
                                    comparisonValueText =comparisonValue + " kg " + funfact.reference[randNo] ;
                                }
                                else{
                                    comparisonValueText =comparisonValue + " g " + funfact.reference[randNo] ;
                                }
                                generatedFunFactText = generatedFunFactText.replace('[comparisonValue]', comparisonValueText);
                            }

                            if(funfact.text.includes('[co2Reference]'))
                            {
                       
                            if(Number(funfact.co2[randNo])>=Number(1000)){
                                funfact.co2[randNo] = funfact.co2[randNo]/1000;
                                funfact.co2[randNo] =funfact.co2[randNo] + " kg ";
                            }
                            else{
                                funfact.co2[randNo] =funfact.co2[randNo] + " g ";
                            }
                            generatedFunFactText = generatedFunFactText.replace('[co2Reference]',funfact.co2[randNo])
                        }

                        }

                        //TYP Energieverbrauch
                        else if (funfact.type === "kcal") {

                            //KCAL Verbrauch
                            const kcalConsumed = (300/60) * durationInM;
                            generatedFunFactText = funfact.text.replace('[kcalConsumption]', kcalConsumed);
                            
                            //berechnung des zweiten Placeholders. --> Wenn vorhanden
                            if (funfact.text.includes('[comparisonValue]')) {
                                randNo = Math.floor(Math.random() * funfact.reference.length);
                                const comparisonValue = funfact.reference[randNo]+ ' ('+ funfact.factor[randNo] + " g" +')' + " hat " + funfact.kcal[randNo] + " kcal";
                                generatedFunFactText = generatedFunFactText.replace('[comparisonValue]', comparisonValue);
                            }
                     
                        }

                        //TYP Dauer
                        else if (funfact.type === "duration") {

                            const co2Text = `${co2savingsInKgRound} kg`;
                            //berechnung des zweiten Placeholders. --> Wenn vorhanden
                                randNo = Math.floor(Math.random() * funfact.reference.length);
                                const calculateDuration =  (co2savingsInKg*1000) / parseFloat(funfact.co2[randNo]);
                                let comparisonValue = Math.round(calculateDuration).toFixed(0);
                       
                                generatedFunFactText = generatedFunFactText.replace('[comparisonObject]', funfact.reference[randNo]);

                                //Wenn weniger als ein Tag benötigt wird, wird "weniger als einen Tag" angezeigt, da ansonsten "0 Tage"
                                if(Number(comparisonValue)>=1){
                                    comparisonValue = comparisonValue + " Tage";
                                   
                                }
                                else {
                                    comparisonValue = " weniger als einen Tag";
                                }
                                generatedFunFactText = generatedFunFactText.replace('[comparisonValue]', comparisonValue);
                                 generatedFunFactText = generatedFunFactText.replace('[placeholderCo2Savings]', co2Text);
                        }
                        
                     //TYP Kilometer
                        else if (funfact.type === "km") {
                            randNo = Math.floor(Math.random() * funfact.reference.length);
                            const percentOfEvent = ((distanceInM) / funfact.distance[randNo])*100;
                            //Runden auf 2 Nachkommastellen
                            const roundedPercent = (Math.round(percentOfEvent * 100) / 100).toFixed(2);
                            generatedFunFactText = funfact.text.replace('[distance]', (distanceInM/1000).toFixed(2));
                            generatedFunFactText = generatedFunFactText.replace('[percentOfEvent]', roundedPercent);
                            generatedFunFactText = generatedFunFactText.replace('[event]', funfact.reference[randNo]);
                        }
                    });
                }
                return generatedFunFactText;
            });
}

export async function calculateTripDistance(booking, traccarId=""){
    let gpsDistance = 0;
    let totalco2SavingsInKg = 0; 

    if (!booking.virtualBooking){
        const startposition =
        {
            latitude: parseFloat(booking.start_lat), 
            longitude: parseFloat(booking.start_lng),
            time: booking.startdate.toISOString()
        }

        const endposition =
        {
            latitude: parseFloat(booking.end_lat), 
            longitude: parseFloat(booking.end_lng),
            time: booking.enddate.toISOString()
        }

        // Get traccar id from booking, if it is not provided
        let resolvedTraccarId = traccarId
        if(resolvedTraccarId==""){
            resolvedTraccarId= booking.bicycleTraccarId;
        }

        const route = await axios({
            method: 'get',
            url: `https://${traccarIp}/api/reports/route?deviceId=${resolvedTraccarId}&from=${booking.startdate.toISOString()}&to=${booking.enddate.toISOString()}`, 
            headers: { 'Authorization': traccarAuth},  
            httpsAgent: new https.Agent({ rejectUnauthorized: false })
            });
        
        const routeSummary = route.data;

        const positions:Array<any> = [];

        for(const position of routeSummary){
            positions.push({
                latitude: position["latitude"],
                longitude: position["longitude"],
                time: moment(position["fixTime"]).toISOString(),
            });
        }

        for (let i = 0; i < positions.length-1; i++){
            gpsDistance+=haversine(positions[i], positions[i+1]);
        }  
        
        if (positions.length > 0){
            // Check distance from first GPS Point to Station
            const beginOffsetKM = haversine(positions[0], startposition);
            const beginOffsetHour = moment(startposition.time).diff(moment(positions[0].time), 'hours', true);
            let avgSpeedKph = Math.abs(beginOffsetKM/beginOffsetHour);
            if (avgSpeedKph < 20) gpsDistance+=beginOffsetKM*1.3
            
            // Check distance from last GPS Point to Station
            const endOffsetKM = haversine(positions[positions.length-1], endposition);
            const endOffsetHour = moment(endposition.time).diff(moment(positions[positions.length-1].time), 'hours', true);
            avgSpeedKph = Math.abs(endOffsetKM/endOffsetHour);
            if (avgSpeedKph < 20) gpsDistance+=endOffsetKM*1.3
        }
    }
    else{
        gpsDistance = (booking.totalDurationInM * 250)/1000;
    }

    const totalDistanceInM = gpsDistance * 1000;
    totalco2SavingsInKg = gpsDistance *  0.21516;

    return {totalDistanceInM, totalco2SavingsInKg};
}

export async function endBookingWorker(bookingId, enddate, end_lat="", end_lng=""){
    let booking = await getBooking(bookingId);
    await attachTrip(booking, enddate);
    
    const user = await getUser(booking.uid);
    booking = await getBooking(bookingId);
    booking.enddate = moment(enddate.toDate());
    
    if(end_lat != "" && end_lng != ""){
        booking.end_lat = end_lat;
        booking.end_lng = end_lng;
    }
    
    const route = await calculateTripDistance(booking);
    let totalAvgSpeedInKMH = (route.totalDistanceInM/1000.0) / (booking.totalDurationInM/60.0);
    if (Number.isNaN(totalAvgSpeedInKMH))
        totalAvgSpeedInKMH = 0;
    
    await booking.ref.update({
        status: "finished",
        enddate: enddate.toDate(),
        enabledRanking: user.enableRanking,
        totalDistanceInM: route.totalDistanceInM,
        totalAvgSpeedInKMH,
        totalco2SavingsInKg: route.totalco2SavingsInKg,
        funfact: await generateFunFact(route.totalDistanceInM, booking.totalDurationInM, route.totalco2SavingsInKg),
        reSynchronize: false,
        end_lat: booking.end_lat,
        end_lng: booking.end_lng
    });
    
    //Update ranking & achievement
    if(!booking.virtualBooking){
        let achievements = {
            totalNumberOfBookings: user.achievements.totalNumberOfBookings + 1,
            totalActiveMonths: user.achievements.totalActiveMonths, // MUST BE UPDATED MONTHLY
            totalDurationInM: user.achievements.totalDurationInM + booking.totalDurationInM,
            totalco2SavingsInKg:  user.achievements.totalco2SavingsInKg + route.totalco2SavingsInKg,
            totalDistanceInM: user.achievements.totalDistanceInM + route.totalDistanceInM,
            monthlyDurationInM:  user.achievements.monthlyDurationInM + + booking.totalDurationInM,
            monthlyco2SavingsInKg: user.achievements.monthlyco2SavingsInKg + route.totalco2SavingsInKg,
            monthlyDistanceInM: user.achievements.monthlyDistanceInM + route.totalDistanceInM,
            monthlyNumberOfBookings: user.achievements.monthlyNumberOfBookings + 1
        }
        await user.ref.update({
            achievements
        })

        if (user.enableRanking){
            await user.ref.update({
                totalco2SavingsInKg: user.totalco2SavingsInKg+route.totalco2SavingsInKg,
                totalDistanceInM: user.totalDistanceInM+route.totalDistanceInM,
                monthlyco2SavingsInKg: user.monthlyco2SavingsInKg+route.totalco2SavingsInKg,
                monthlyDistanceInM: user.monthlyDistanceInM + route.totalDistanceInM
                }
            )
        }
    }
    return true;
}

export async function sendEmail(to:string, subject:string, html:string){
    const mailConfigSnapshot = await admin.firestore().collection('config').doc("mail").get();
    const mailConfig = mailConfigSnapshot.data();

    if(mailConfig){
        const transporter = nodemailer.createTransport({
            host: mailConfig.host,
            port: mailConfig.port,
            secure: mailConfig.secure,
            auth: {
                user: mailConfig.user, 
                pass: mailConfig.pass, 
            },
        });
    
        return transporter.sendMail({
            from: mailConfig.from, 
            to: to, 
            subject: subject,
            html: html
        });
    }
    else{
        throw(Error("Configuration document with id 'mail' could not be found in collection 'config'"))
    }
}


export async function getDistanceReport(uid, ranking){
    let totalDistanceInM = 0;
    let totalDurationInM =0;
    let monthlyDistanceInM =0;
    let monthlyDurationInM=0;
    const month = (new Date()).getMonth();
    let bookingSnapshot;
    if(ranking){ 
        bookingSnapshot= await admin.firestore().collection('bookings').where("uid","==",uid).where("enabledRanking","==",true).get();
        
    }
    else{
        bookingSnapshot= await admin.firestore().collection('bookings').where("uid","==",uid).get();
    }
    bookingSnapshot.forEach(booking => {
        
        totalDistanceInM+=booking.data().totalDistanceInM;
        totalDurationInM+=booking.data().totalDurationInM;
        if(month===((booking.data().enddate).toDate()).getMonth()){
            monthlyDistanceInM+=booking.data().totalDistanceInM;
            monthlyDurationInM+=booking.data().totalDurationInM;
        }
    });
   
    //console.log(distance);
    return {totalDistanceInM,totalDurationInM,monthlyDistanceInM,monthlyDurationInM};   
    
}
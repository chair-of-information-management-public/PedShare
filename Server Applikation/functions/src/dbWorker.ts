import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as moment from 'moment-timezone';
import * as nodeutil from 'util';
moment.tz.setDefault("Europe/Berlin");

import * as util from "./util";

// Asks MM for permission if new user uses private e-mail adress
// Triggered when new user is created
export let onCreateUser = functions.region('europe-west1').firestore
    .document('users/{uid}')
    .onCreate(async (userSnapshot, context) => {

    const user = util.unpackUser(userSnapshot);
    await onCreateUserWorker(user);       
});

export async function onCreateUserWorker(user){
    
        // If user is not automatically enabled, ask mobility manager to enable account
        if (!user.accountEnabled){
            const companySnapshot = await admin.firestore().collection('companies').doc(user.companyId).get();
            const company = companySnapshot.data();
    
            if(company){
                const subject =  "Freigabe des Accounts <" + user.email + "> für PedShare";
                const html =  
                    `<p>Lieber Mobility-Manager,</p>
                    <p>der User <b>${user.name} (${user.email})</b> möchte gerne die PedShare-App Ihres Unternehmens nutzen.<br> 
                    Sie können die Nutzung durch Klicken des folgenden Links autorisieren:<br>
                    https://${util.firebaseUrl}/authorizeAccount?uid=${user.id}</p>
                    <p>Viele Grüße vom PedShare Team</p>
                    `;
                await util.sendEmail(company.mobilityManagerMail, subject, html);
            }
        }
}

// Resets montly ranking data and adds month-related achievements to user
// Triggered on 1. of month midnight
export let resetMonthlyRanking = functions.region('europe-west1').pubsub.schedule('0 0 1 * *')
.timeZone("Europe/Berlin")
.onRun(async (context) => {
    await resetMonthlyRankingWorker();
    return null;
});

export async function resetMonthlyRankingWorker() {
    const companies = await admin.firestore().collection('companies').get();
    for (const company of companies.docs){
        const userSnapshots = await admin.firestore().collection('users').where('companyId', '==', company.id).get();
        
        // Determine Ranking Victory
        let winnerId = undefined;
        let winnerDistance = 0;
        
        userSnapshots.forEach(async userSnapshot => {
            const user = await util.unpackUser(userSnapshot);
            if(user.monthlyDistanceInM > winnerDistance){
                winnerDistance = user.monthlyDistanceInM;
                winnerId = user.id;
            }
        });

        if(winnerId){
            console.log("Winner is user #" + winnerId + " with a distance of " + winnerDistance + "m"); 
        }
        else
        {
            console.log("No activies this month :-(");
        }
            
        userSnapshots.forEach(async userSnapshot => {
            const user = await util.unpackUser(userSnapshot);
            const achievements = {
                //Copys total achievement values
                totalNumberOfBookings: user.achievements.totalNumberOfBookings, 
                totalDurationInM: user.achievements.totalDurationInM,
                totalco2SavingsInKg: user.achievements.totalco2SavingsInKg,
                totalDistanceInM: user.achievements.totalDistanceInM,
                //Updates active month
                totalActiveMonths: (user.achievements.monthlyNumberOfBookings > 0)? user.achievements.totalActiveMonths + 1: user.achievements.totalActiveMonths, 
                //Resets monthly counters
                monthlyDurationInM: 0,
                monthlyco2SavingsInKg: 0,
                monthlyDistanceInM: 0,
                monthlyNumberOfBookings: 0 
            }
            
            user.ref.update(
                {
                    achievements,
                    //Updates monthly wins
                    totalMonthlyWins: (user.id===winnerId)? user.totalMonthlyWins+1: user.totalMonthlyWins
                }
            )
            
            console.log("Updated User #" + user.id);
        });
    }
}

// Makes sure that bookings that have been finished offline, will be resynchronized with Traccar data
// Triggered every day
export let resyncBookings = functions.region('europe-west1').pubsub.schedule('0 0 * * *')
.timeZone("Europe/Berlin")
.onRun(async (context) => {
    await resyncBookingsWorker();
    return null;
});

export async function resyncBookingsWorker(){
    try{
        const bookingSnapshots =  await admin.firestore().collection('bookings').where('reSynchronize', '==', true).get();
        for (const booking of bookingSnapshots.docs)
        {
            await util.endBookingWorker(booking.id, moment(booking.data().enddate.toDate()));
        }
    }
    catch(ex) {
       console.log({success: false, message: nodeutil.inspect(ex)});
    }    
}
import * as functions from 'firebase-functions';
import * as moment from 'moment-timezone';
import * as util from './util';
import * as dbWorker from './dbWorker';

moment.tz.setDefault("Europe/Berlin");

export let isUserAbleToBookTest = functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.query;
    if(!data.start || !data.end || !data.uid) {
        return response.send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }

    data.start = moment(data.start, 'DD.MM.YYYY hh:mm').toDate()
    data.end = moment(data.end, 'DD.MM.YYYY hh:mm').toDate()

    util.isUserAbleToBook(data.start, data.end, data.uid).then(able => {
        return response.send({status: "success", isUserAbleToBook: able});
    }).catch((err) => {
        return response.send({status: "error", message: err})
    });     
});

export let generateFunFactTest = functions.region('europe-west1').https.onRequest(async (request:any, response:any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.query;
    if (!data.distanceInM  || !data.durationInM || !data.co2savingsInKg) {
        return response.send({ success: false, message: "Nicht genug oder fehlerhafte Daten übergeben!" });
    }
    try{
        const generatedFunFactText = await util.generateFunFact(data.distanceInM, data.durationInM, data.co2savingsInKg, data.id);
        return response.send({ success: true, funfact: generatedFunFactText});
    }
    catch(ex) {
        return response.send({success: false, message: ex})
    }          
});


export let calculateTripDistanceTest = functions.region('europe-west1').https.onRequest(async (request:any, response:any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.query;
    if (!data.bookingId ) {
        return response.send({ success: false, message: "Nicht genug oder fehlerhafte Daten übergeben!" });
    }
    try{
        const booking = await util.getBooking(data.bookingId);
        const bookingDistance = await util.calculateTripDistance(booking);
        return response.send({ success: true, bookingDistance});
    }
    catch(ex) {
        return response.send({success: false, message: ex})
    }          
});

export let onCreateUserTest = functions.region('europe-west1').https.onRequest(async (request:any, response:any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.query;
    if (!data.uid ) {
        return response.send({ success: false, message: "Nicht genug oder fehlerhafte Daten übergeben!" });
    }
    let user = await util.getUser(data.uid);
    
    dbWorker.onCreateUserWorker(user).then(able => {
        return response.send({success: true, user});
    }).catch((err) => {
        return response.send({success: false, message: err})
    });    
});

export let resetMonthlyRankingTest = functions.region('europe-west1').https.onRequest(async (request:any, response:any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');

    dbWorker.resetMonthlyRankingWorker().then(() => {
        return response.send({success: true});
    }).catch((err) => {
        return response.send({success: false, message: err})
    }); 
});

export let resyncBookingsTest = functions.region('europe-west1').https.onRequest(async (request:any, response:any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');

    dbWorker.resyncBookingsWorker().then(()=>{
        return response.send({success: true});
    }).catch((err) => {
        return response.send({success: false, message: err})
    }); 
});

import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as moment from 'moment-timezone';
import * as util from './util';
import * as nodeutil from 'util'

moment.tz.setDefault("Europe/Berlin");
admin.initializeApp(functions.config().firebase);

import * as test from './test';
import * as dbWorker from './dbWorker';
exports.test = test;
exports.dbWorker = dbWorker;

// Make service functions only available for emulator-environment
import * as service from './service'
if(process.env.FUNCTIONS_EMULATOR)
    exports.service = service;
     

export const getAvailableBicyles = functions.region('europe-west1').https.onRequest((request: any, response: any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
 
    const data = request.query; 

    if(!data.startstationId || !data.endstationId || !data.startdate || !data.enddate || !data.companyId) {
        response.status(200).send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"})
        return;
    }
    data.startdate = moment(data.startdate, 'DD.MM.YYYY HH:mm').toDate()
    data.enddate = moment(data.enddate, 'DD.MM.YYYY HH:mm').toDate()
    
    if (data.startdate > data.enddate) {
        response.status(200).send({status: "error", errormessage: "Das Enddatum liegt vor dem Startdatum!"});
        return;
    }

    //Get all bikes from a company
    admin.firestore().collection('bicycles').orderBy("pedshareId").where("companyId", "==", data.companyId).get()
    .then(bicycles => {
        const availabilityCheck = bicycles.docs.map(
            function(bike){
                return util.isBikeAvailable(data.startstationId, data.endstationId, data.startdate, data.enddate, bike.id)
            }
        )
        
        // Check requested availablity for all bikes
        Promise.all(availabilityCheck).then(availabilities =>
        {
            const availableBicycles: any[] = [];
            bicycles.docs.forEach((bicycle, index) => {
                if(availabilities[index])
                    availableBicycles.push({bicycleId : bicycle.id, name: bicycle.data().name, pedshareId: bicycle.data().pedshareId}); 
            });
            response.status(200).send({status: "success", availableBicycles: availableBicycles});
        }).catch(err => {
            return response.send({status: "error", error: err})
        })
    }).catch(err => {
        return response.send({status: "error", error: nodeutil.inspect(err)})
});
});

export const makeBooking = functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');

    const data = request.body;
    if(!data.startdate || !data.enddate || !data.uid || !data.startstationId || !data.endstationId || !data.bicycleId ) {
        return response.send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }
    moment.tz.setDefault("Europe/Berlin");
    data.startdate = moment(data.startdate, 'DD.MM.YYYY HH:mm').toDate()
    data.enddate = moment(data.enddate, 'DD.MM.YYYY HH:mm').toDate()
    
    util.isBookingValid(data.startstationId, data.endstationId, data.startdate, data.enddate, data.bicycleId, data.uid).
        then(valid => {
            if(valid)
            {
                Promise.all([
                    admin.firestore().collection('bicycleStations').doc(data.startstationId).get(),
                    admin.firestore().collection('bicycleStations').doc(data.endstationId).get(),
                    admin.firestore().collection('bicycles').doc(data.bicycleId).get()
                ]).then(async db => {
                    const startStation = db[0].data()
                    const endStation = db[1].data()
                    const bike = db[2].data()
                    if(bike && startStation && endStation){
                        await admin.firestore().collection('bookings').doc()
                        .set({
                            uid: data.uid,
                            startdate: data.startdate,
                            enddate: data.enddate,
                            bicycleId: data.bicycleId,
                            bicycleName: bike.name,
                            bicyclePedshareId: bike.pedshareId,
                            bicycleTraccarId: bike.lock.traccarId,
                            startstationId: data.startstationId,
                            startstationName: startStation.name,
                            start_lat: startStation.latitude,
                            start_lng: startStation.longitude,
                            endstationId: data.endstationId, 
                            endstationName: endStation.name,
                            end_lat: endStation.latitude,
                            end_lng: endStation.longitude,
                            status: "planned",
                            totalDistanceInM: 0,
                            totalDurationInM: 0,
                            virtualBooking: bike.lock.name === "dummylock"
                            }
                        );
                        return response.send({status: "success"});
                    }
                    else{
                        return response.send({status: "error", error: "Data for startstation, endstation or bike are missing."})
                    }
                }).catch(err => {
                    return response.send({status: "error", error: nodeutil.inspect(err)})
                })
            }
            else {
                return response.send({status: "error", errormessage: "Keine Buchungskapazitäten für dieses Fahrrad!"});
        }})     
        .catch(err => {
            return response.send({status: "error", error: nodeutil.inspect(err)})
    });
});

// Attaches a the traccar track to the last trip of a booking
export const attachTripToBooking = functions.region('europe-west1').https.onRequest(async (request: any, response: any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
       
    const data = request.body;

    if(!data.bookingId || !data.enddate) {
        return response.send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }

    const enddate = moment(data.enddate, 'DD.MM.YYYY HH:mm')

    try{
        let booking = await util.getBooking(data.bookingId);
        await util.attachTrip(booking, enddate);
        return response.send({status: "success"});
    }
    catch(err){
        return response.send({status: "error", error: nodeutil.inspect(err)});
    }
});

export const endBooking = functions.region('europe-west1').https.onRequest(async (request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.body;
    if(!data.bookingId || !data.enddate ) {
        return response.send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }
    moment.tz.setDefault("Europe/Berlin");

    //deprecated app version don't submit seconds
    if (data.enddate.length==16){
        data.enddate += ':00';
    }

    const enddate = moment(data.enddate, 'DD.MM.YYYY HH:mm:ss'); 
    
    try{
        if(data.end_lat && data.end_lng)
            await util.endBookingWorker(data.bookingId, enddate, data.end_lat, data.end_lng);
        else           
            await util.endBookingWorker(data.bookingId, enddate);
        return response.send({status: "success"});
    }
    catch (err){
        return response.send({status: "error", error: nodeutil.inspect(err)})
    }
});

export const deleteBooking = functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.body;
    
    if(!data.bookingId) {
        return response.send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }
    
    admin.firestore().collection("bookings").doc(data.bookingId).get().then(async bookingSnapshot => {
        await bookingSnapshot.ref.collection("trips").get().then(tripSnapshots => {
            const deleteJobs = Array();
            tripSnapshots.forEach(trip => {
                deleteJobs.push(trip.ref.delete());                    
            })
            
            return Promise.all(deleteJobs)
            .then(() => {
                bookingSnapshot.ref.delete()
                .then(() => {return response.send({status: "success"})})
                .catch((err) => {return response.send({status: "error", err:err})});
                })
            .catch((err) => {return response.send({status: "error", err:err})});
        })     
    }).then(() => {
        return response.send({status: "success"})
    }).catch((err) => {
        return response.send({status: "error", err: nodeutil.inspect(err)})
    });
})

//todo -> refactor
export const extendBooking = functions.region('europe-west1').https.onRequest(async (request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
   
    const data = request.body;
    if(!data.newEnddate || !data.bookingId) {
        return response.send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }

    moment.tz.setDefault("Europe/Berlin");
    data.newEnddate = moment(data.newEnddate, 'DD.MM.YYYY hh:mm').toDate()

    await util.getBooking(data.bookingId).then(async booking => {
        await Promise.all([
            //Check if user is able to book
            util.isUserAbleToBook(booking.enddate, data.newEnddate, booking.uid),
            //Check if bike is available for booking
            admin.firestore().collection('bookings').where("bicycleId", "==", booking.bicycleId).
                where('startdate', ">=", moment().toDate()).
                orderBy('startdate').
                limit(1).
                get().
                then(bookings => {
                    if(bookings.empty)//No future booking
                        return {works: true};
                    else
                    {
                        const nextStart = moment(bookings.docs[0].data().startdate.toDate());
                        if (nextStart >= data.newEnddate)
                            return {works: true}
                        else 
                            return {works: false, availableUntil: nextStart.format('DD.MM.YYYY HH:mm')}
                    }
                })
            ]).then(result => {
                if (result[0] === false)
                    return response.send({status: "success", works: false, msg: "Ihre Anfrage überschneidet sich mit Ihrer nächsten Buchung. Löschen Sie bitte Ihre nächste Buchung und versuchen Sie es erneut."});
                else
                    if(result[1].works){
                        booking.ref.update({enddate: data.newEnddate}).then(() => {
                            return response.send({status: "success", works: true});
                        }).catch(error => {return response.send({status: "error", errormessage: error})})
                    }
                    else
                        return response.send({status: "success", works: false, msg: "Ihr Fahrrad ist leider nur bis " + result[1].availableUntil + " verfügbar. Bitte wählen Sie ein früheres Enddatum."});
            }).catch(error => { return response.send({status: "error", errormessage: nodeutil.inspect(error)})});
        });
});

export const isBikeAvailableTest = functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');
    
    const data = request.query;
    if(!data.startstation || !data.endstation || !data.start || !data.end || !data.bicycleId) {
        return response.send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }

    data.start = moment(data.start, 'DD.MM.YYYY hh:mm').toDate()
    data.end = moment(data.end, 'DD.MM.YYYY hh:mm').toDate()

    if (data.bookingId === undefined)
        data.bookingId = ""

    util.isBikeAvailable(data.startstation, data.endstation, data.start, data.end, data.bicycleId, data.bookingId).then(available => {
        return response.send({"status": "success", "isBikeAvailable": available});
    }).catch((err) => {
        return response.send({"status": "error", "message": nodeutil.inspect(err)})
    });       
});

export const updateBooking = functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');

    const data = request.body;
    
    if(!data.startstation || !data.endstation || !data.start || !data.end || !data.bicycleId || !data.endstationName || !data.uid || !data.bookingId) {
        return response.send({status: "error", errormessage: "Nicht genug oder fehlerhafte Daten übergeben!"});
    }

    data.start = moment(data.start, 'DD.MM.YYYY hh:mm').toDate()
    data.end = moment(data.end, 'DD.MM.YYYY hh:mm').toDate()

    util.isBookingValid(data.startstation, data.endstation, data.start, data.end, data.bicycleId, data.uid, data.bookingId).then(async available => {
        if(available === true){ 
            const endstation = await (await admin.firestore().collection('bicycleStations').doc(data.endstation).get()).data();
            if (endstation)
                await admin.firestore().collection('bookings').doc(data.bookingId).update({
                    endstationId: data.endstation,
                    endstationName: data.endstationName,
                    startdate: data.start,
                    enddate: data.end,
                    end_lat: endstation.latitude,
                    end_lng: endstation.longitude
                })
            else{
                return response.send({status: "error", message: "Missing data for endstation."})
            }
        }  
        return response.send({status: "success", bookingPossible: available});     
    }).catch((err) => {
        return response.send({status: "error", message: nodeutil.inspect(err)})
    });      
});     

export const authorizeAccount = functions.region('europe-west1').https.onRequest((request: any, response : any) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Content-Type');

    const data = request.query; 

    if(!data.uid){
        return response.send("Der Link ist ungültig. Wenden Sie sich an den PedShare Administrator");
    }
        
    util.getUser(data.uid).then(async user => {
        const subject =  "Freigabe Deines PedShare Accounts";
        const html =  
            `<p>Lieber PedShare-User,</p>
            <p>Dein Account wurde vom Mobility Manager Deines Unternehmens freigeschaltet.<br> 
            Du kannst Dich nun mit Deinen selbst gewählten Zugangsdaten in der PedShare App anmelden.
            <br> Wir wünschen allzeit gute Fahrt!</p>
            <p>Viele Grüße vom PedShare Team</p>
            `;
        
        if (!user.accountEnabled){
            await user.ref.update({accountEnabled: true});
            await util.sendEmail(user.email, subject, html);
        }
        
        return response.send(`Vielen Dank! Der Account des Users <b>${user.name} (${user.email})</b> wurde aktiviert.<br> Der User wird automatisch per E-Mail über die Freischaltung des Accounts benachrichtigt.`);
    }).catch( () => {
        return response.send("Der Link ist ungültig. Wenden Sie sich bitte an den PedShare Administrator.");
    })  
});
